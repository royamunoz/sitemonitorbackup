﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public interface ILicenceService
    {
        IEnumerable<LicenceState> GetLicenceStates(bool includeSiteInfo);
        Task UpdateLicence(LicenceState state);
    }
}