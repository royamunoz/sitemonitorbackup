﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.ViewModels
{
    public class LicenceStateViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Estación")]
        public string Name { get; set; }

        [Display(Name = "Fecha de Expiración")]
        public DateTime ExpirationDate { get; set; }
        [Display(Name = "Última Actualización")]
        public DateTime UpdatedOn { get; set; }

        [Display(Name = "Días Restantes")]
        public int RemainingDays { get; set; }
    }
}
