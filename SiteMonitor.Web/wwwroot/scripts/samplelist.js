if (!window) {

    var window = exports.window = {};
}
window.samplesList = [
    {
        "name": "Data Grid",
        "directory": "Grid",
        "category": "Grids",
        "type": "update",
        "samples": [
            {
                "url": "GridOverview",
                "name": "Overview",
                "category": "Data Grid",
                "uid": "004445",
                "order": 0,
                "component": "Grid",
                "type": "new",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultFunctionalities",
                "name": "Default Functionalities",
                "category": "Data Grid",
                "uid": "00440",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Grouping",
                "name": "Grouping",
                "category": "Data Grid",
                "uid": "00441",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "GridLines",
                "name": "GridLines",
                "category": "Data Grid",
                "uid": "00442",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Events",
                "name": "Events",
                "category": "Data Grid",
                "uid": "00443",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Clipboard",
                "name": "Clipboard",
                "category": "Data Grid",
                "uid": "00444",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ContextMenu",
                "name": "Context Menu",
                "category": "Data Grid",
                "uid": "00445",
                "order": 0,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultScrolling",
                "name": "Default Scrolling",
                "category": "Scrolling",
                "uid": "00446",
                "order": 1,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "VirtualScrolling",
                "name": "Virtual Scrolling",
                "category": "Scrolling",
                "uid": "00447",
                "order": 1,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "LocalData",
                "name": "List Binding",
                "category": "DataBinding",
                "uid": "00448",
                "order": 2,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "RemoteData",
                "name": "Remote Data",
                "category": "DataBinding",
                "uid": "00449",
                "order": 2,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "UrlAdaptor",
                "name": "URL Adaptor",
                "category": "DataBinding",
                "uid": "08559", "order": 2,
                "type": "update",
                "component": "Grid",
                "dir": "Grid",
                "parentId": "05"
            },
            {
                "url": "AutoWrap",
                "name": "AutoWrap Column Cells",
                "category": "Columns",
                "uid": "004410",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ShowHideColumn",
                "name": "Show or Hide Column",
                "category": "Columns",
                "uid": "004411",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ColumnTemplate",
                "name": "Column Template",
                "category": "Columns",
                "uid": "004412",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "StackedHeaders",
                "name": "Stacked Header",
                "category": "Columns",
                "uid": "004413",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Reorder",
                "name": "Reorder",
                "category": "Columns",
                "uid": "004414",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ColumnChooser",
                "name": "Column Chooser",
                "category": "Columns",
                "uid": "004415",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "HeaderTemplate",
                "name": "Header Template",
                "category": "Columns",
                "uid": "004416",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ColumnResize",
                "name": "Column Resize",
                "category": "Columns",
                "uid": "004417",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ColumnSpanning",
                "name": "Column Spanning",
                "category": "Columns",
                "uid": "004418",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "FrozenRows",
                "name": "Frozen Rows And Columns",
                "category": "Columns",
                "uid": "004419",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "ColumnMenu",
                "name": "Column Menu",
                "category": "Columns",
                "uid": "004420",
                "order": 3,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "RowTemplate",
                "name": "Row Template",
                "category": "Rows",
                "uid": "004421",
                "order": 4,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DetailTemplate",
                "name": "Detail Template",
                "category": "Rows",
                "uid": "004422",
                "order": 4,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "RowHover",
                "name": "Row Hover",
                "category": "Rows",
                "uid": "004423",
                "order": 4,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "RowHeight",
                "name": "Row Height",
                "category": "Rows",
                "uid": "004424",
                "order": 4,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "RowDragAndDrop",
                "name": "Row Drag And Drop",
                "category": "Rows",
                "uid": "004425",
                "order": 4,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "MultiSorting",
                "name": "Multi Sorting",
                "category": "Sorting",
                "uid": "004426",
                "order": 5,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "SortingAPI",
                "name": "Sorting API",
                "category": "Sorting",
                "uid": "004427",
                "order": 5,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultFiltering",
                "name": "Default Filtering",
                "category": "Filtering",
                "uid": "004428",
                "order": 6,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "FilterMenu",
                "name": "Filter Menu",
                "category": "Filtering",
                "uid": "004429",
                "order": 6,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Search",
                "name": "Search",
                "category": "Filtering",
                "uid": "004430",
                "order": 6,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultPaging",
                "name": "Default Paging",
                "category": "Paging",
                "uid": "004431",
                "order": 7,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "PagingAPI",
                "name": "Paging API",
                "category": "Paging",
                "uid": "004432",
                "order": 7,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultSelection",
                "name": "Default Selection",
                "category": "Selection",
                "uid": "004433",
                "order": 8,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "SelectionAPI",
                "name": "Selection API",
                "category": "Selection",
                "uid": "004434",
                "order": 8,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "CheckboxSelection",
                "name": "Checkbox Selection",
                "category": "Selection",
                "uid": "004435",
                "order": 8,
                "type": "update",
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultAggregate",
                "name": "Default Aggregate",
                "category": "Aggregates",
                "uid": "004436",
                "order": 9,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "GroupAndCaptionAggregate",
                "name": "Group and Caption Aggregate",
                "category": "Aggregates",
                "uid": "004437",
                "order": 9,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "InlineEditing",
                "name": "Inline Editing",
                "category": "Editing",
                "uid": "004438",
                "order": 10,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DialogEditing",
                "name": "Dialog Editing",
                "category": "Editing",
                "uid": "004439",
                "order": 10,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "BatchEditing",
                "name": "Batch Editing",
                "category": "Editing",
                "uid": "004440",
                "order": 10,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "CommandColumn",
                "name": "Command Column",
                "category": "Editing",
                "uid": "004441",
                "order": 10,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "DefaultExporting",
                "name": "Default Exporting",
                "category": "Exporting",
                "uid": "004442",
                "order": 11,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "AdvancedExporting",
                "name": "Advanced Exporting",
                "category": "Exporting",
                "uid": "004443",
                "order": 11,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            },
            {
                "url": "Print",
                "name": "Print",
                "category": "Exporting",
                "uid": "004444",
                "order": 11,
                "component": "Grid",
                "dir": "Grid",
                "parentId": "04"
            }
        ],
        "order": 1,
        "uid": "04"
    },
    {
        "name": "Pivot Grid",
        "directory": "PivotView",
        "category": "Grids",
        "type": "preview",
        "samples": [{
            "url": "Default",
            "name": "Default Functionalities",
            "category": "Pivot Grid",
            "uid": "00343441",
            "order": 0,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "LocalData",
            "name": "Local Data",
            "category": "Data Binding",
            "uid": "00343442",
            "order": 1,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "RemoteData",
            "name": "Remote Data",
            "category": "Data Binding",
            "uid": "00343443",
            "order": 1,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "FieldList",
            "name": "Field List",
            "category": "User Interaction",
            "uid": "00343444",
            "order": 2,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "GroupingBar",
            "name": "Grouping Bar",
            "category": "User Interaction",
            "uid": "00343445",
            "order": 2,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        // {
        //     "url": "Interaction",
        //     "name": "Grid Interaction",
        //     "category": "User Interaction",
        //     "uid": "00343446",
        //     "order": 2,
        //     "component": "PivotView",
        //     "dir": "PivotView",
        //     "parentId": "037"
        // },
        // {
        //     "url": "DrillOptions",
        //     "name": "Drill Options",
        //     "category": "User Interaction",
        //     "uid": "00343447",
        //     "order": 2,
        //     "component": "PivotView",
        //     "dir": "PivotView",
        //     "parentId": "037"
        // },
        {
            "url": "CalculatedField",
            "name": "Calculated Field",
            "category": "Formula",
            "uid": "00343448",
            "order": 3,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "Aggregation",
            "name": "Aggregation",
            "category": "Formula",
            "uid": "00343449",
            "order": 3,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "Sorting",
            "name": "Default Sorting",
            "category": "Sorting",
            "uid": "00343450",
            "order": 4,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "ValueSorting",
            "name": "Value Sorting",
            "category": "Sorting",
            "uid": "00343451",
            "order": 4,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "Filtering",
            "name": "Default Filtering",
            "category": "Filtering",
            "uid": "00343452",
            "order": 5,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "Chart",
            "name": "Chart",
            "category": "Integration",
            "uid": "00343453",
            "order": 6,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        },
        {
            "url": "Exporting",
            "name": "Export",
            "category": "Miscellaneous",
            "uid": "00343454",
            "order": 7,
            "component": "PivotView",
            "dir": "PivotView",
            "parentId": "037"
        }
        ],
        "order": 1,
        "uid": "037"
      },
  {
      "name": "Chart",
      "directory": "Chart",
      "category": "Data Visualization",
      "type": "update",
      "samples": [
        {
            "url": "Line",
            "name": "Line",
            "category": "Line Charts",
            "uid": "00000",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Spline",
            "name": "Spline",
            "category": "Line Charts",
            "uid": "00001",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StepLine",
            "name": "Step Line",
            "category": "Line Charts",
            "uid": "00002",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "DashedLine",
            "name": "Dashed Line",
            "category": "Line Charts",
            "uid": "00003",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "InversedSpline",
            "name": "Inversed Spline",
            "category": "Line Charts",
            "uid": "00004",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "LineZone",
            "name": "Line Zone",
            "category": "Line Charts",
            "uid": "00005",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "MultiColoredLine",
            "name": "Multi Colored Line",
            "category": "Line Charts",
            "uid": "00006",
            "order": 0,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Area",
            "name": "Area",
            "category": "Area Charts",
            "uid": "00007",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "SplineArea",
            "name": "Spline Area",
            "category": "Area Charts",
            "uid": "00008",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StepArea",
            "name": "Step Area",
            "category": "Area Charts",
            "uid": "00009",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RangeArea",
            "name": "Range Area",
            "category": "Area Charts",
            "uid": "000010",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedArea",
            "name": "Stacked Area",
            "category": "Area Charts",
            "uid": "000011",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedArea100",
            "name": "100% Stacked Area",
            "category": "Area Charts",
            "uid": "000012",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "AreaEmpty",
            "name": "Area Empty",
            "category": "Area Charts",
            "uid": "000013",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "AreaZone",
            "name": "Area Zone",
            "category": "Area Charts",
            "uid": "000014",
            "order": 1,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Column",
            "name": "Column",
            "category": "Bar Charts",
            "uid": "000015",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RoundedColumn",
            "name": "Rounded Column",
            "category": "Bar Charts",
            "uid": "000016",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "ColumnPlacement",
            "name": "Column Placement",
            "category": "Bar Charts",
            "uid": "000017",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RangeColumn",
            "name": "Range Column",
            "category": "Bar Charts",
            "uid": "000018",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RangeBar",
            "name": "Range Bar",
            "category": "Bar Charts",
            "uid": "000019",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Bar",
            "name": "Bar",
            "category": "Bar Charts",
            "uid": "000020",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedColumn",
            "name": "Stacked Column",
            "category": "Bar Charts",
            "uid": "000021",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedColumn100",
            "name": "100% Stacked Column",
            "category": "Bar Charts",
            "uid": "000022",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedBar",
            "name": "Stacked Bar",
            "category": "Bar Charts",
            "uid": "000023",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StackedBar100",
            "name": "100% Stacked Bar",
            "category": "Bar Charts",
            "uid": "000024",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "NegativeStack",
            "name": "Negative Stack",
            "category": "Bar Charts",
            "uid": "000025",
            "order": 2,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Scatter",
            "name": "Scatter",
            "category": "Scatter and Bubble",
            "uid": "000026",
            "order": 3,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Bubble",
            "name": "Bubble",
            "category": "Scatter and Bubble",
            "uid": "000027",
            "order": 3,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Waterfall",
            "name": "Waterfall",
            "category": "Other Types",
            "uid": "000028",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Histogram",
            "name": "Histogram",
            "category": "Other Types",
            "type": "new",
            "uid": "000092",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "BoxandWhisker",
            "name": "Box and Whisker",
            "category": "Other Types",
            "uid": "000029",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "ErrorBar",
            "name": "ErrorBar",
            "category": "Other Types",
            "uid": "000030",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "TrendLines",
            "name": "Trendlines",
            "category": "Other Types",
            "uid": "000031",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "CombinationSeries",
            "name": "Combination Series",
            "category": "Other Types",
            "uid": "000032",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PareTo",
            "name": "Pareto Chart",
            "category": "Other Types",
            "uid": "000033",
            "order": 4,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Hilo",
            "name": "Hilo",
            "category": "Financial Charts",
            "uid": "000034",
            "order": 5,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "HiloOpenClose",
            "name": "Hilo Open Close",
            "category": "Financial Charts",
            "type": "update",
            "uid": "000035",
            "order": 5,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Candle",
            "name": "Candle",
            "category": "Financial Charts",
            "type": "update",
            "uid": "000036",
            "order": 5,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "ADIChart",
            "name": "Accumulation Distribution",
            "category": "Technical Indicators",
            "uid": "000037",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "AtrChart",
            "name": "Atr",
            "category": "Technical Indicators",
            "uid": "000038",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "BollingerChart",
            "name": "Bollinger",
            "category": "Technical Indicators",
            "uid": "000039",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "EmaChart",
            "name": "Ema",
            "category": "Technical Indicators",
            "uid": "000040",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "MacdChart",
            "name": "Macd",
            "category": "Technical Indicators",
            "uid": "000041",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "MomentumChart",
            "name": "Momentum",
            "category": "Technical Indicators",
            "uid": "000042",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RsiChart",
            "name": "Rsi",
            "category": "Technical Indicators",
            "uid": "000043",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "SmaChart",
            "name": "Sma",
            "category": "Technical Indicators",
            "uid": "000044",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "StochasticChart",
            "name": "Stochastic",
            "category": "Technical Indicators",
            "uid": "000045",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "TmaChart",
            "name": "Tma",
            "category": "Technical Indicators",
            "uid": "000046",
            "order": 6,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "BenchMark",
            "name": "BenchMark",
            "category": "Performance",
            "uid": "000047",
            "order": 7,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Pie",
            "name": "Pie",
            "category": "Accumulation Chart",
            "uid": "000048",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Doughnut",
            "name": "Doughnut",
            "category": "Accumulation Chart",
            "uid": "000049",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Pyramid",
            "name": "Pyramid",
            "category": "Accumulation Chart",
            "uid": "000050",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Funnel",
            "name": "Funnel",
            "category": "Accumulation Chart",
            "uid": "000051",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PieLegend",
            "name": "Pie with Legend",
            "category": "Accumulation Chart",
            "uid": "000052",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "SemiPie",
            "name": "Semi Pie",
            "category": "Accumulation Chart",
            "uid": "000053",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "SmartLabel",
            "name": "Smart Labels",
            "category": "Accumulation Chart",
            "uid": "000054",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Drilldown",
            "name": "Drilldown",
            "category": "Accumulation Chart",
            "uid": "000055",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Grouping",
            "name": "Grouping",
            "category": "Accumulation Chart",
            "type": "update",
            "uid": "000056",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PieEmpty",
            "name": "Empty Points",
            "category": "Accumulation Chart",
            "uid": "000057",
            "order": 8,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarLine",
            "name": "Line",
            "category": "Polar Charts",
            "uid": "000058",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarSpline",
            "name": "Spline",
            "category": "Polar Charts",
            "uid": "000059",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarArea",
            "name": "Area",
            "category": "Polar Charts",
            "uid": "000060",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarStackedArea",
            "name": "Stacked Area",
            "category": "Polar Charts",
            "uid": "000061",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarScatter",
            "name": "Scatter",
            "category": "Polar Charts",
            "uid": "000062",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarColumn",
            "name": "Column",
            "category": "Polar Charts",
            "uid": "000063",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarWindRose",
            "name": "Wind Rose",
            "category": "Polar Charts",
            "uid": "000064",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "PolarRangeColumn",
            "name": "Range Column",
            "category": "Polar Charts",
            "uid": "000065",
            "order": 9,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "LocalData",
            "name": "Local Data",
            "category": "Data Binding",
            "uid": "000066",
            "order": 10,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RemoteData",
            "name": "Remote Data",
            "category": "Data Binding",
            "uid": "000067",
            "order": 10,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "NumericAxis",
            "name": "Numeric Axis",
            "category": "Chart Axes",
            "uid": "000068",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "DateTimeAxis",
            "name": "DateTime Axis",
            "category": "Chart Axes",
            "uid": "000069",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "DateTimeCategoryAxis",
            "name": "DateTime Category Axis",
            "category": "Chart Axes",
            "uid": "000070",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "CategoryAxis",
            "name": "Category Axis",
            "category": "Chart Axes",
            "uid": "000071",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "IndexedCategoryAxis",
            "name": "Indexed Category Axis",
            "category": "Chart Axes",
            "uid": "000072",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "LogAxis",
            "name": "Log Axis",
            "category": "Chart Axes",
            "uid": "000073",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "MultipleAxis",
            "name": "Multiple Axis",
            "category": "Chart Axes",
            "uid": "000074",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "InversedAxis",
            "name": "Inversed Axis",
            "category": "Chart Axes",
            "uid": "000075",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Stripline",
            "name": "Strip Line",
            "category": "Chart Axes",
            "uid": "000076",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "SmartAxisLabel",
            "name": "Smart Axis Labels",
            "category": "Chart Axes",
            "uid": "000077",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Multilevellabel",
            "name": "Multilevel labels",
            "category": "Chart Axes",
            "uid": "000078",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "AxisCrossing",
            "name": "Axis Crossing",
            "category": "Chart Axes",
            "uid": "000079",
            "order": 11,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Sorting",
            "name": "Sorting",
            "category": "Chart Customization",
            "uid": "000080",
            "order": 12,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Symbols",
            "name": "Symbols",
            "category": "Chart Customization",
            "uid": "000081",
            "order": 12,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },

        {
            "url": "DataLabelTemplate",
            "name": "DataLabel Template",
            "category": "Chart Customization",
            "uid": "000082",
            "order": 12,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Vertical",
            "name": "Vertical",
            "category": "Chart Customization",
            "uid": "000083",
            "order": 12,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "EmptyPoint",
            "name": "Empty Points",
            "category": "Chart Customization",
            "uid": "000084",
            "order": 12,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Print",
            "name": "Print",
            "category": "Print and Export",
            "uid": "000085",
            "order": 13,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Export",
            "name": "Export",
            "category": "Print and Export",
            "uid": "000086",
            "order": 13,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Selection",
            "name": "Selection",
            "category": "User Interaction",
            "uid": "000087",
            "order": 14,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "RangeSelection",
            "name": "Range Selection",
            "category": "User Interaction",
            "uid": "000088",
            "order": 14,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Crosshair",
            "name": "Crosshair",
            "category": "User Interaction",
            "uid": "000089",
            "order": 14,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Trackball",
            "name": "Trackball",
            "category": "User Interaction",
            "uid": "000090",
            "order": 14,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"
        },
        {
            "url": "Zooming",
            "name": "Zooming and Panning",
            "category": "User Interaction",
            "type": "update",
            "uid": "000091",
            "order": 14,
            "component": "Chart",
            "dir": "Chart",
            "parentId": "00"

        }
      ],
      "order": 0,
      "uid": "00"
  },
  {
      "name": "Diagram",
      "directory": "Diagram",
      "category": "Data Visualization",
      "type": "preview",
      "samples": [
          {
              "url": "FlowChart",
              "name": "Default Functionalities",
              "category": "Getting Started",
              "uid": "00301",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "DiagramShapes",
              "name": "Shapes",
              "category": "Getting Started",
              "uid": "00302",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "Nodes",
              "name": "Nodes",
              "category": "Getting Started",
              "uid": "00303",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "Connector",
              "name": "Connectors",
              "category": "Getting Started",
              "uid": "00304",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "Annotation",
              "name": "Annotations",
              "category": "Getting Started",
              "uid": "00305",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "Port",
              "name": "Ports",
              "category": "Getting Started",
              "uid": "00306",
              "order": 0,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "DrawingTools",
              "name": "Drawing Tools",
              "category": "User Interaction",
              "uid": "00307",
              "order": 1,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "KeyboardInteraction",
              "name": "Keyboard Interaction",
              "category": "User Interaction",
              "uid": "00308",
              "order": 1,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "UserHandle",
              "name": "User Handles",
              "category": "User Interaction",
              "uid": "00309",
              "order": 1,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "SymbolPalette",
              "name": "Symbol Palette",
              "category": "User Interaction",
              "uid": "00310",
              "order": 1,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "Overview",
              "name": "Overview Panel",
              "category": "User Interaction",
              "uid": "00311",
              "order": 1,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "Serialization",
              "name": "Save and Load",
              "category": "Print and Export",
              "uid": "00312",
              "order": 2,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "PrintandExport",
              "name": "Print and Export",
              "category": "Print and Export",
              "uid": "00313",
              "order": 2,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "HierarchicalTree",
              "name": "Hierarchical Tree",
              "category": "Automatic Layouts",
              "uid": "00314",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "OrganizationalChartLayout",
              "name": "Organizational Chart",
              "category": "Automatic Layouts",
              "uid": "00315",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },

          {
              "url": "RadialTree",
              "name": "Radial Tree",
              "category": "Automatic Layouts",
              "uid": "00316",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "MindMap",
              "name": "Mind Map",
              "category": "Automatic Layouts",
              "uid": "00317",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "SymmetricLayout",
              "name": "Symmetric Layout",
              "category": "Automatic Layouts",
              "uid": "00318",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "RTLTree",
              "name": "RTL Tree",
              "category": "Automatic Layouts",
              "uid": "00329",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "PertChart",
              "name": "Pert Chart",
              "category": "Automatic Layouts",
              "uid": "00328",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "ComplexHierarchicalLayout",
              "name": "Complex Hierarchical Tree",
              "category": "Data Visualization",
              "uid": "00319",
              "order": 3,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          },
          {
              "url": "LocalData",
              "name": "Local Data",
              "category": "Data Binding",
              "uid": "00321",
              "order": 4,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "00"
          },
          {
              "url": "VennDiagram",
              "name": "Venn Diagram",
              "category": "Static Diagram",
              "uid": "00327",
              "order": 5,
              "component": "Diagram",
              "dir": "Diagram",
              "parentId": "01"
          }
      ],
      "order": 0,
      "uid": "00"
  },
  {
      "name": "Maps",
      "directory": "Maps",
      "category": "Data Visualization",
      "type": "update",
      "samples": [
        {
            "url": "Default",
            "name": "Default Functionalities",
            "category": "Default",
            "uid": "00110",
            "order": 0,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "ChangeProjection",
            "name": "Projection",
            "category": "Features",
            "uid": "00111",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "MultiLayer",
            "name": "Multi-layers",
            "category": "Features",
            "uid": "00112",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Marker",
            "name": "Marker",
            "category": "Features",
            "uid": "00113",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "MarkerTemplate",
            "name": "MarkerTemplate",
            "category": "Features",
            "uid": "00114",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "MapLabel",
            "name": "Labels",
            "category": "Features",
            "uid": "00115",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Bubble",
            "name": "Bubble",
            "category": "Features",
            "uid": "00116",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "NavigationLine",
            "name": "Navigation Lines",
            "category": "Features",
            "uid": "00117",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Legend",
            "name": "Legend",
            "category": "Features",
            "uid": "00118",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Annotation",
            "name": "Annotations",
            "category": "Features",
            "uid": "00119",
            "order": 1,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Tooltip",
            "name": "Tooltip",
            "category": "User Interaction",
            "uid": "001110",
            "order": 3,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Selection",
            "name": "Selection & Highlight",
            "category": "User Interaction",
            "uid": "001111",
            "order": 3,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Zooming",
            "name": "Zooming & Panning",
            "category": "User Interaction",
            "uid": "001112",
            "order": 3,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Drilldown",
            "name": "Drill Down",
            "category": "User Interaction",
            "uid": "001113",
            "order": 3,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Print",
            "name": "Print",
            "category": "Print of Export",
            "uid": "001114",
            "order": 4,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Export",
            "name": "Export",
            "category": "Print of Export",
            "uid": "001115",
            "order": 4,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Heatmap",
            "name": "Heat Map",
            "category": "Use Cases",
            "uid": "001116",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Curvedlines",
            "name": "Flight routes",
            "category": "Use Cases",
            "uid": "001117",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Earthquake",
            "name": "Earthquake indication",
            "category": "Use Cases",
            "uid": "001118",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "Highlight",
            "name": "Highlighted region",
            "category": "Use Cases",
            "uid": "001119",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "MapPie",
            "name": "Map with Pie chart",
            "category": "Use Cases",
            "uid": "001120",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        },
        {
            "url": "SeatSelection",
            "name": "Bus seat booking",
            "category": "Use Cases",
            "uid": "001121",
            "order": 5,
            "component": "Maps",
            "dir": "Maps",
            "parentId": "01"
        }
      ],
      "order": 0,
      "uid": "01"
  },
    {
        "name": "HeatMap",
        "directory": "HeatMap",
        "category": "Data Visualization",
        "type": "preview",
        "samples": [{
            "url": "Default",
            "name": "Default Functionalities",
            "category": "HeatMap",
            "uid": "10220",
            "order": 0,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "RowArray",
            "name": "Row",
            "category": "Data Binding",
            "uid": "10221",
            "order": 1,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "CellArray",
            "name": "Cell",
            "category": "Data Binding",
            "uid": "10222",
            "order": 1,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "RowJson",
            "name": "JSON Row",
            "category": "Data Binding",
            "uid": "10223",
            "order": 1,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "CellJson",
            "name": "JSON Cell",
            "category": "Data Binding",
            "uid": "10224",
            "order": 1,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "EmptyPoint",
            "name": "Empty Points",
            "category": "Features",
            "uid": "10225",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "Inversed",
            "name": "Inversed Axis",
            "category": "Features",
            "uid": "10226",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "Opposed",
            "name": "Opposed Axis",
            "category": "Features",
            "uid": "10227",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "Legend",
            "name": "Legend Placement",
            "category": "Features",
            "uid": "10228",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "LargeData",
            "name": "Large Data",
            "category": "Features",
            "uid": "10229",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "Palette",
            "name": "Palette Mode",
            "category": "Features",
            "uid": "10230",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "RenderMode",
            "name": "Rendering Mode",
            "category": "Features",
            "uid": "10231",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }, {
            "url": "TooltipTemplate",
            "name": "Tooltip Template",
            "category": "Features",
            "uid": "10232",
            "order": 2,
            "component": "HeatMap",
            "dir": "HeatMap",
            "parentId": "38"
        }],
        "order": 0,
        "uid": "38"
    },
  {
      "name": "TreeMap",
      "directory": "TreeMap",
      "category": "Data Visualization",
      "type": "preview",
      "samples": [
          {
              "url": "Default",
              "name": "Default Functionalities",
              "category": "TreeMap",
              "uid": "00220",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "Layout",
              "name": "Layout",
              "category": "TreeMap",
              "uid": "00221",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "Drilldown",
              "name": "Drilldown",
              "category": "TreeMap",
              "uid": "00222",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "Customization",
              "name": "Customization",
              "category": "TreeMap",
              "uid": "00223",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "DataLabel",
              "name": "DataLabel",
              "category": "TreeMap",
              "uid": "00224",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "Tooltip",
              "name": "Tooltip",
              "category": "TreeMap",
              "uid": "00225",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "Legend",
              "name": "Legend",
              "category": "TreeMap",
              "uid": "00226",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "SelectionAndHighlight",
              "name": "Selection & Highlight",
              "category": "TreeMap",
              "uid": "00227",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "PrintAndExport",
              "name": "Print & Export",
              "category": "TreeMap",
              "uid": "00228",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          },
          {
              "url": "TreeMapWithPie",
              "name": "TreeMap With Pie",
              "category": "TreeMap",
              "uid": "00229",
              "order": 0,
              "component": "TreeMap",
              "dir": "TreeMap",
              "parentId": "02"
          }
      ],
      "order": 0,
      "uid": "02"
  },
  {
      "name": "CircularGauge",
      "directory": "CircularGauge",
      "category": "Data Visualization",
      "samples": [
        {
            "url": "Default",
            "name": "Default Functionalities",
            "category": "CircularGauge",
            "uid": "00220",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "Range",
            "name": "Range",
            "category": "CircularGauge",
            "uid": "00221",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "Ticks",
            "name": "Ticks and Labels",
            "category": "CircularGauge",
            "uid": "00222",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "Annotation",
            "name": "Annotation",
            "category": "CircularGauge",
            "uid": "00223",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "Customization",
            "name": "Gauge Customization",
            "category": "CircularGauge",
            "uid": "00224",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "DirectionCompass",
            "name": "Direction Compass",
            "category": "CircularGauge",
            "uid": "00225",
            "order": 0,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "PointerImage",
            "name": "Pointer Image",
            "category": "Pointer",
            "uid": "00226",
            "order": 1,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "PointerCustomization",
            "name": "Pointer Customization",
            "category": "Pointer",
            "uid": "00227",
            "order": 1,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "MultipleAxis",
            "name": "Multiple Axis",
            "category": "Axes",
            "uid": "00228",
            "order": 2,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "PointerDrag",
            "name": "PointerDrag",
            "category": "User Interaction",
            "uid": "00229",
            "order": 3,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "Tooltip",
            "name": "Tooltip",
            "category": "User Interaction",
            "uid": "002210",
            "order": 3,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        },
        {
            "url": "DataSample",
            "name": "Data Sample",
            "category": "Live",
            "uid": "002211",
            "order": 4,
            "component": "CircularGauge",
            "dir": "CircularGauge",
            "parentId": "03"
        }
      ],
      "order": 0,
      "uid": "03"
  },
  {
      "name": "LinearGauge",
      "directory": "LinearGauge",
      "category": "Data Visualization",
      "samples": [
        {
            "url": "Default",
            "name": "Default Functionalities",
            "category": "LinearGauge",
            "uid": "00330",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Container",
            "name": "Container",
            "category": "LinearGauge",
            "uid": "00331",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Ranges",
            "name": "Ranges",
            "category": "LinearGauge",
            "uid": "00332",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "DataSample",
            "name": "Data Sample",
            "category": "LinearGauge",
            "uid": "00333",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Axes",
            "name": "Axes and Pointers",
            "category": "LinearGauge",
            "uid": "00334",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Annotation",
            "name": "Annotation",
            "category": "LinearGauge",
            "uid": "00335",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Tooltip",
            "name": "Tooltip",
            "category": "LinearGauge",
            "uid": "00336",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        },
        {
            "url": "Styles",
            "name": "Styles",
            "category": "LinearGauge",
            "uid": "00337",
            "order": 0,
            "component": "LinearGauge",
            "dir": "LinearGauge",
            "parentId": "04"
        }
      ],
      "order": 0,
      "uid": "04"
  },
  {
      "name": "RangeNavigator",
      "directory": "RangeNavigator",
      "category": "Data Visualization",
      "type": "preview",
      "samples": [
          {
              "url": "Default",
              "name": "Default",
              "category": "Range Navigator",
              "uid": "00430",
              "order": 0,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "LightWeight",
              "name": "Lightweight",
              "category": "Range Navigator",
              "uid": "00431",
              "order": 0,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "DateTime",
              "name": "DateTime Axis",
              "category": "Axis",
              "uid": "00432",
              "order": 1,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "Double",
              "name": "Numeric Axis",
              "category": "Axis",
              "uid": "00433",
              "order": 1,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "Logarithmic",
              "name": "Logarithmic Axis",
              "category": "Axis",
              "uid": "00434",
              "order": 1,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "Multilevel",
              "name": "Multilevel Labels",
              "category": "Axis",
              "uid": "00435",
              "order": 1,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "PeriodDefault",
              "name": "Default",
              "category": "Period Selector",
              "uid": "00436",
              "order": 2,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "PeriodStock",
              "name": "Stock Chart",
              "category": "Period Selector",
              "uid": "00437",
              "order": 2,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "EmptyData",
              "name": "Empty Points",
              "category": "Customization",
              "uid": "00438",
              "order": 3,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "Filter",
              "name": "Filter",
              "category": "Customization",
              "uid": "00439",
              "order": 3,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },

          {
              "url": "Export",
              "name": "Print and Export",
              "category": "Export",
              "uid": "00441",
              "order": 4,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
          {
              "url": "rtl",
              "name": "RTL",
              "category": "RTL",
              "uid": "00442",
              "order": 5,
              "component": "RangeNavigator",
              "dir": "RangeNavigator",
              "parentId": "07"
          },
      ],
      "order": 0,
      "uid": "07"
  },
  {
      "name": "Sparkline",
      "directory": "SparkLine",
      "category": "Data Visualization",
      "type": "preview",
      "samples": [
          {
              "url": "Default",
              "name": "Default Functionalities",
              "category": "Sparkline",
              "uid": "00220",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "SeriesTypes",
              "name": "Series Types",
              "category": "Sparkline",
              "uid": "00221",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "AxisTypes",
              "name": "Axis Types",
              "category": "Sparkline",
              "uid": "00222",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "SparkGrid",
              "name": "Sparkline with Grid",
              "category": "SparkLine",
              "uid": "00223",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "Customization",
              "name": "Customization",
              "category": "SparkLine",
              "uid": "00224",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "LiveUpdate",
              "name": "Live Update",
              "category": "SparkLine",
              "uid": "00225",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          },
          {
              "url": "RangeBand",
              "name": "Range Band",
              "category": "SparkLine",
              "uid": "00226",
              "order": 0,
              "component": "SparkLine",
              "dir": "SparkLine",
              "parentId": "05"
          }
      ],
      "order": 0,
      "uid": "05"
  },
  {

      "name": "SmithChart",
      "directory": "SmithChart",
      "category": "Data Visualization",
      "type": "preview",
      "samples": [
          {
              "url": "Default",
              "name": "Default Functionalities",
              "category": "SmithChart",
              "uid": "00330",
              "order": 0,
              "component": "SmithChart",
              "dir": "SmithChart",
              "parentId": "06"
          },
          {
              "url": "Customization",
              "name": "Customization",
              "category": "SmithChart",
              "uid": "00331",
              "order": 0,
              "component": "SmithChart",
              "dir": "SmithChart",
              "parentId": "06"
          },
          {
              "url": "PrintAndExport",
              "name": "Print & Export",
              "category": "SmithChart",
              "uid": "00332",
              "order": 0,
              "component": "SmithChart",
              "dir": "SmithChart",
              "parentId": "06"
          }
      ],
      "order": 0,
      "uid": "06"
  },

  {
      "name": "Schedule",
      "directory": "Schedule",
      "category": "Calendar",
      "type": "preview",
      "samples": [
        {
            "url": "Default",
            "name": "Default Functionalities",
            "category": "Schedule",
            "uid": "00550",
            "order": 0,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "DefaultEvents",
            "name": "Normal",
            "category": "Appointments",
            "uid": "00551",
            "order": 1,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "RecurrenceEvents",
            "name": "Recurrence",
            "category": "Appointments",
            "uid": "00552",
            "order": 1,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Timezone",
            "name": "Timezone",
            "category": "Appointments",
            "uid": "00553",
            "order": 1,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Views",
            "name": "Basic",
            "category": "Views",
            "uid": "00554",
            "order": 2,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Agenda",
            "name": "Agenda",
            "category": "Views",
            "uid": "00555",
            "order": 2,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "MonthAgenda",
            "name": "Month Agenda",
            "category": "Views",
            "uid": "00556",
            "order": 2,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "ViewBasedSettings",
            "name": "View-based Settings",
            "category": "Views",
            "type": "update",
            "uid": "00557",
            "order": 2,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "ExtendedViews",
            "name": "Extended views",
            "category": "Views",
            "type": "new",
            "uid": "005529",
            "order": 2,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Resources",
            "name": "Fare Calendar",
            "category": "Resources",
            "type": "new",
            "uid": "005530",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "Group",
            "name": "Grouping",
            "category": "Resources",
            "type": "new",
            "uid": "005531",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "GroupByDate",
            "name": "Group By Date",
            "category": "Resources",
            "type": "new",
            "uid": "005532",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "GroupByChild",
            "name": "Group By Level",
            "category": "Resources",
            "type": "new",
            "uid": "005533",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "GroupEditing",
            "name": "Grouped Events",
            "category": "Resources",
            "type": "new",
            "uid": "005534",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "GroupCustomWorkdays",
            "name": "Customized Workdays",
            "category": "Resources",
            "type": "new",
            "uid": "005535",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "AddRemoveResources",
            "name": "Show/Hide Resources",
            "category": "Resources",
            "type": "new",
            "uid": "005536",
            "order": 3,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "00"
        },
        {
            "url": "LocalData",
            "name": "List Binding",
            "category": "Data Binding",
            "uid": "00558",
            "order": 4,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "RemoteData",
            "name": "Remote Data",
            "category": "Data Binding",
            "uid": "00559",
            "order": 4,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "CellTemplate",
            "name": "Cells",
            "category": "Template",
            "uid": "005510",
            "order": 5,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "DateHeader",
            "name": "Date Header",
            "category": "Template",
            "uid": "005511",
            "order": 5,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "EventTemplate",
            "name": "Events",
            "category": "Template",
            "uid": "005512",
            "order": 5,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Tooltip",
            "name": "Tooltip",
            "category": "Template",
            "uid": "005513",
            "order": 5,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "EditorValidation",
            "name": "Field Validation",
            "category": "Editor",
            "uid": "005514",
            "order": 6,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "EditorCustomField",
            "name": "Additional Fields",
            "category": "Editor",
            "uid": "005515",
            "order": 6,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "EditorTemplate",
            "name": "Editor Template",
            "category": "Editor",
            "uid": "005516",
            "order": 6,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "TimeScale",
            "name": "Time Scale",
            "category": "Schedule",
            "type": "new",
            "uid": "005537",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "HeaderBar",
            "name": "Header Bar",
            "category": "Customization",
            "uid": "005517",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "ScrollTime",
            "name": "Scroll Time",
            "category": "Customization",
            "uid": "005518",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "WorkDays",
            "name": "Work Days",
            "category": "Customization",
            "uid": "005519",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "HideWeekend",
            "name": "Hide Weekend",
            "category": "Customization",
            "uid": "005520",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "WorkHours",
            "name": "Work Hours",
            "category": "Customization",
            "uid": "005521",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "DayHourLimit",
            "name": "Day Hour Limit",
            "category": "Customization",
            "uid": "005522",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "CellDimension",
            "name": "Cell Dimension",
            "category": "Customization",
            "uid": "005523",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "ReadonlyEvents",
            "name": "Readonly Events",
            "category": "Customization",
            "uid": "005524",
            "order": 7,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "RecurrenceRuleGenerator",
            "name": "Rule Generator",
            "category": "Recurrence editor",
            "uid": "005525",
            "order": 8,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "RecurrencePopulateRule",
            "name": "Populate Rule",
            "category": "Recurrence editor",
            "uid": "005526",
            "order": 8,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "KeyboardInteraction",
            "name": "Keyboard Interaction",
            "category": "Miscellaneous",
            "uid": "005527",
            "order": 9,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        },
        {
            "url": "Events",
            "name": "Events",
            "category": "Miscellaneous",
            "uid": "005528",
            "order": 9,
            "component": "Schedule",
            "dir": "Schedule",
            "parentId": "05"
        }
      ],
      "order": 2,
      "uid": "05"
  },
  {
      "name": "Calendar",
      "directory": "Calendar",
      "category": "Calendar",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Calendar",
            "uid": "00660",
            "order": 0,
            "component": "Calendar",
            "dir": "Calendar",
            "parentId": "06"
        },
        {
            "url": "DisabledDates",
            "name": "Disabled Dates",
            "category": "Calendar",
            "uid": "00661",
            "order": 0,
            "component": "Calendar",
            "dir": "Calendar",
            "parentId": "06"
        },
        {
            "url": "DateRange",
            "name": "Date Range",
            "category": "Calendar",
            "uid": "00662",
            "order": 0,
            "component": "Calendar",
            "dir": "Calendar",
            "parentId": "06"
        },
        {
            "url": "SpecialDates",
            "name": "Special Dates",
            "category": "Calendar",
            "uid": "00663",
            "order": 0,
            "component": "Calendar",
            "dir": "Calendar",
            "parentId": "06"
        },
        {
            "url": "Globalization",
            "name": "Globalization",
            "category": "Calendar",
            "uid": "00664",
            "order": 0,
            "component": "Calendar",
            "dir": "Calendar",
            "parentId": "06"
        }
      ],
      "order": 2,
      "uid": "06"
  },
  {
      "name": "AutoComplete",
      "directory": "AutoComplete",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "AutoComplete",
            "uid": "00770",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "GroupingAndIcon",
            "name": "Grouping and Icon",
            "category": "AutoComplete",
            "uid": "00771",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "DataBinding",
            "name": "Data Binding",
            "category": "AutoComplete",
            "uid": "00772",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "AutoComplete",
            "uid": "00773",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "Highlight",
            "name": "Highlight",
            "category": "AutoComplete",
            "uid": "00774",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "CustomFiltering",
            "name": "Custom Filtering",
            "category": "AutoComplete",
            "uid": "00775",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        },
        {
            "url": "DiacriticsFiltering",
            "name": "Diacritics Filtering",
            "category": "AutoComplete",
            "uid": "00776",
            "order": 0,
            "component": "AutoComplete",
            "dir": "AutoComplete",
            "parentId": "07"
        }
      ],
      "order": 3,
      "uid": "07"
  },
  {
      "name": "Button",
      "directory": "Button",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "DefaultFunctionalities",
            "category": "Button",
            "uid": "00880",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08"
        },
        {
            "url": "ButtonGroup",
            "name": "ButtonGroup",
            "category": "Button",
            "uid": "00881",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08",
            "type": "preview"
        },
        {
            "url": "CheckBox",
            "name": "CheckBox",
            "category": "Button",
            "uid": "00882",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08"
        },
        {
            "url": "RadioButton",
            "name": "RadioButton",
            "category": "Button",
            "uid": "00883",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08"
        },
        {
            "url": "DropDownButton",
            "name": "DropDownButton",
            "category": "Button",
            "uid": "00884",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08"
        },
        {
            "url": "SplitButton",
            "name": "SplitButton",
            "category": "Button",
            "uid": "00885",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08"
        },
        {
            "url": "Switch",
            "name": "Switch",
            "category": "Button",
            "uid": "00886",
            "order": 0,
            "component": "Button",
            "dir": "Button",
            "parentId": "08",
            "type": "preview"
        }
      ],
      "order": 3,
      "uid": "08"
  },
  {
      "name": "ComboBox",
      "directory": "ComboBox",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "ComboBox",
            "uid": "00990",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "GroupingAndIcon",
            "name": "Grouping and Icon",
            "category": "ComboBox",
            "uid": "00991",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "DataBinding",
            "name": "Data Binding",
            "category": "ComboBox",
            "uid": "00992",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "CustomValue",
            "name": "Custom Values",
            "category": "ComboBox",
            "uid": "00993",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "ComboBox",
            "uid": "00994",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "Filtering",
            "name": "Filtering",
            "category": "ComboBox",
            "uid": "00995",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "Cascading",
            "name": "Cascading",
            "category": "ComboBox",
            "uid": "00996",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        },
        {
            "url": "DiacriticsFiltering",
            "name": "Diacritics Filtering",
            "category": "ComboBox",
            "uid": "00997",
            "order": 0,
            "component": "ComboBox",
            "dir": "ComboBox",
            "parentId": "09"
        }
      ],
      "order": 3,
      "uid": "09"
  },
  {
      "name": "DatePicker",
      "directory": "DatePicker",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "DatePicker",
            "uid": "0010100",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "DisabledDates",
            "name": "Disabled Dates",
            "category": "DatePicker",
            "uid": "0010101",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "DateRange",
            "name": "Date Range",
            "category": "DatePicker",
            "uid": "0010102",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "Format",
            "name": "Format",
            "category": "DatePicker",
            "uid": "0010103",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "SpecialDates",
            "name": "Special Dates",
            "category": "DatePicker",
            "uid": "0010104",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "Globalization",
            "name": "Globalization",
            "category": "DatePicker",
            "uid": "0010105",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010"
        },
        {
            "url": "DatePickerFor",
            "name": "DatepickerFor",
            "category": "DatePicker",
            "uid": "0010106",
            "order": 0,
            "component": "DatePicker",
            "dir": "DatePicker",
            "parentId": "010",
            "type": "new"
        }
      ],
      "order": 3,
      "uid": "010"
  },
  {
      "name": "DateRangePicker",
      "directory": "DateRangePicker",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "DateRangePicker",
            "uid": "0011110",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011"
        },
        {
            "url": "DateRange",
            "name": "Date Range",
            "category": "DateRangePicker",
            "uid": "0011111",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011"
        },
        {
            "url": "DaySpan",
            "name": "Day Span",
            "category": "DateRangePicker",
            "uid": "0011112",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011"
        },
        {
            "url": "Format",
            "name": "Format",
            "category": "DateRangePicker",
            "uid": "0011113",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011",
            "type": "update"
        },
        {
            "url": "Globalization",
            "name": "Globalization",
            "category": "DateRangePicker",
            "uid": "0011114",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011"
        },
        {
            "url": "Presets",
            "name": "Preset Ranges",
            "category": "DateRangePicker",
            "uid": "0011115",
            "order": 0,
            "component": "DateRangePicker",
            "dir": "DateRangePicker",
            "parentId": "011",
            "type": "new"
        }
      ],
      "order": 3,
      "uid": "011"
  },
  {
      "name": "DateTimePicker",
      "directory": "DateTimePicker",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "DateTimePicker",
            "uid": "0012120",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "DisabledDates",
            "name": "Disabled Dates",
            "category": "DateTimePicker",
            "uid": "0012121",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "DateTimeRange",
            "name": "DateTime Range",
            "category": "DateTimePicker",
            "uid": "0012122",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "Format",
            "name": "Format",
            "category": "DateTimePicker",
            "uid": "0012123",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "SpecialDates",
            "name": "Special Dates",
            "category": "DateTimePicker",
            "uid": "0012124",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "Globalization",
            "name": "Globalization",
            "category": "DateTimePicker",
            "uid": "0012125",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012"
        },
        {
            "url": "DateTimePickerFor",
            "name": "DateTimePickerFor",
            "category": "DateTimePicker",
            "uid": "0012126",
            "order": 0,
            "component": "DateTimePicker",
            "dir": "DateTimePicker",
            "parentId": "012",
            "type": "new"
        }
      ],
      "order": 3,
      "uid": "012"
  },
  {
      "name": "DropDownList",
      "directory": "DropDownList",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "DropDownList",
            "uid": "0013130",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "GroupingAndIcon",
            "name": "Grouping and Icon",
            "category": "DropDownList",
            "uid": "0013131",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "DataBinding",
            "name": "Data Binding",
            "category": "DropDownList",
            "uid": "0013132",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "DropDownList",
            "uid": "0013133",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "Filtering",
            "name": "Filtering",
            "category": "DropDownList",
            "uid": "0013134",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "Cascading",
            "name": "Cascading",
            "category": "DropDownList",
            "uid": "0013135",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "Inline",
            "name": "Inline",
            "category": "DropDownList",
            "uid": "0013136",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        },
        {
            "url": "DiacriticsFiltering",
            "name": "Diacritics Filtering",
            "category": "DropDownList",
            "uid": "0013137",
            "order": 0,
            "component": "DropDownList",
            "dir": "DropDownList",
            "parentId": "013"
        }
      ],
      "order": 3,
      "uid": "013"
  },
  {
      "name": "MultiSelect",
      "directory": "MultiSelect",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "MultiSelect",
            "uid": "0014140",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "DataBinding",
            "name": "Data Binding",
            "category": "MultiSelect",
            "uid": "0014141",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "GroupingAndIcon",
            "name": "Grouping",
            "category": "MultiSelect",
            "uid": "0014142",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "MultiSelect",
            "uid": "0014143",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "Filtering",
            "name": "Filtering",
            "category": "MultiSelect",
            "uid": "0014144",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "CustomValue",
            "name": "Custom Values",
            "category": "MultiSelect",
            "uid": "0014145",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "ChipCustomization",
            "name": "Chip Customization",
            "category": "MultiSelect",
            "uid": "0014146",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "DiacriticsFiltering",
            "name": "Diacritics Filtering",
            "category": "MultiSelect",
            "uid": "0014147",
            "order": 0,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "CheckBox",
            "name": "CheckBox",
            "category": "CheckBox",
            "uid": "0014148",
            "order": 1,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        },
        {
            "url": "SelectionLimit",
            "name": "Selection Limit",
            "category": "CheckBox",
            "uid": "0014149",
            "order": 1,
            "component": "MultiSelect",
            "dir": "MultiSelect",
            "parentId": "014"
        }
      ],
      "order": 3,
      "uid": "014"
  },
  {
      "name": "MaskedTextBox",
      "directory": "MaskedTextBox",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "MaskedTextBox",
            "uid": "0015150",
            "order": 0,
            "component": "MaskedTextBox",
            "dir": "MaskedTextBox",
            "parentId": "015"
        },
        {
            "url": "CustomMask",
            "name": "Custom Mask",
            "category": "MaskedTextBox",
            "uid": "0015151",
            "order": 0,
            "component": "MaskedTextBox",
            "dir": "MaskedTextBox",
            "parentId": "015"
        },
        {
            "url": "Formats",
            "name": "Formats",
            "category": "MaskedTextBox",
            "uid": "0015152",
            "order": 0,
            "component": "MaskedTextBox",
            "dir": "MaskedTextBox",
            "parentId": "015"
        }
      ],
      "order": 3,
      "uid": "015"
  },
  {
      "name": "NumericTextBox",
      "directory": "NumericTextBox",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "NumericTextBox",
            "uid": "0016160",
            "order": 0,
            "component": "NumericTextBox",
            "dir": "NumericTextBox",
            "parentId": "016"
        },
        {
            "url": "RangeValidation",
            "name": "Range Validation",
            "category": "NumericTextBox",
            "uid": "0016161",
            "order": 0,
            "component": "NumericTextBox",
            "dir": "NumericTextBox",
            "parentId": "016"
        },
        {
            "url": "Internationalization",
            "name": "Internationalization",
            "category": "NumericTextBox",
            "uid": "0016162",
            "order": 0,
            "component": "NumericTextBox",
            "dir": "NumericTextBox",
            "parentId": "016"
        },
        {
            "url": "CustomFormat",
            "name": "Custom Format",
            "category": "NumericTextBox",
            "uid": "0016163",
            "order": 0,
            "component": "NumericTextBox",
            "dir": "NumericTextBox",
            "parentId": "016"
        },
        {
            "url": "RestrictDecimals",
            "name": "Restrict Decimals",
            "category": "NumericTextBox",
            "uid": "0016164",
            "order": 0,
            "component": "NumericTextBox",
            "dir": "NumericTextBox",
            "parentId": "016"
        }
      ],
      "order": 3,
      "uid": "016"
  },
  {
      "name": "Slider",
      "directory": "Slider",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Slider",
            "uid": "0017170",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "Ticks",
            "name": "Ticks",
            "category": "Slider",
            "uid": "0017171",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "Tooltip",
            "name": "Tooltip",
            "category": "Slider",
            "uid": "0017172",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017",
            "type": "update"
        },
        {
            "url": "VerticalOrientation",
            "name": "Vertical Orientation",
            "category": "Slider",
            "uid": "0017173",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017",
            "type": "update"
        },
        {
            "url": "Limits",
            "name": "Limits",
            "category": "Slider",
            "uid": "0017174",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017",
            "type": "new"
        },
        {
            "url": "Formatting",
            "name": "Formatting",
            "category": "Slider",
            "uid": "0017175",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "API",
            "name": "API",
            "category": "Slider",
            "uid": "0017176",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "Events",
            "name": "Events",
            "category": "Slider",
            "uid": "0017177",
            "order": 0,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "ThumbCustomization",
            "name": "Thumb",
            "category": "Customization",
            "uid": "0017178",
            "order": 1,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "BarCustomization",
            "name": "Bar",
            "category": "Customization",
            "uid": "0017179",
            "order": 1,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "TicksCustomization",
            "name": "Ticks",
            "category": "Customization",
            "uid": "0017180",
            "order": 1,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "TooltipCustomization",
            "name": "Tooltip",
            "category": "Customization",
            "uid": "00171781",
            "order": 1,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        },
        {
            "url": "AzurePricing",
            "name": "Cloud Pricing",
            "category": "Use Case",
            "uid": "00171782",
            "order": 2,
            "component": "Slider",
            "dir": "Slider",
            "parentId": "017"
        }
      ],
      "order": 3,
      "uid": "017"
  },
  {
      "name": "TextBoxes",
      "directory": "TextBoxes",
      "category": "Editors",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "TextBoxes",
            "uid": "0018180",
            "order": 0,
            "component": "TextBoxes",
            "dir": "TextBoxes",
            "parentId": "018"
        }
      ],
      "order": 3,
      "uid": "018"
  },
  {
      "name": "TimePicker",
      "directory": "TimePicker",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "TimePicker",
            "uid": "0019190",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019"
        },
        {
            "url": "Format",
            "name": "Format",
            "category": "TimePicker",
            "uid": "0019191",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019"
        },
        {
            "url": "TimeRange",
            "name": "Time Range",
            "category": "TimePicker",
            "uid": "0019192",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019"
        },
        {
            "url": "TimeDuration",
            "name": "Time Duration",
            "category": "TimePicker",
            "uid": "0019193",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019"
        },
        {
            "url": "Globalization",
            "name": "Globalization",
            "category": "TimePicker",
            "uid": "0019194",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019"
        },
        {
            "url": "TimePickerFor",
            "name": "TimePickerFor",
            "category": "TimePicker",
            "uid": "0019195",
            "order": 0,
            "component": "TimePicker",
            "dir": "TimePicker",
            "parentId": "019",
            "type": "new"
        }
      ],
      "order": 3,
      "uid": "019"
  },
  {
      "name": "Uploader",
      "directory": "Uploader",
      "category": "Editors",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Uploader",
            "uid": "0020200",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "ChunkUpload",
            "name": "Chunk Upload",
            "category": "Uploader",
            "uid": "0020210",
            "type": "new",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "Uploader",
            "uid": "0020201",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "PreloadFiles",
            "name": "Preload Files",
            "category": "Uploader",
            "uid": "0020202",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "FileValidation",
            "name": "File Validation",
            "category": "Uploader",
            "uid": "0020203",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "ImagePreview",
            "name": "Image Preview",
            "category": "Uploader",
            "uid": "0020204",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "FormSupport",
            "name": "Form Support",
            "category": "Uploader",
            "uid": "0020206",
            "type": "new",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "CustomDropArea",
            "name": "Custom Drop Area",
            "category": "Uploader",
            "uid": "0020207",
            "type": "new",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "Uploader",
            "uid": "0020205",
            "order": 0,
            "component": "Uploader",
            "dir": "Uploader",
            "parentId": "020"
        }
      ],
      "order": 3,
      "uid": "020"
  },
  {
      "name": "DocumentEditor",
      "directory": "DocumentEditor",
      "category": "Editors",
      "type": "preview",
      "samples": [{
          "url": "Default",
          "name": "Default Functionalities",
          "category": "DocumentEditor",
          "order": 1,
          "component": "DocumentEditor",
          "dir": "DocumentEditor",
          "parentId": "035",
          "uid": "0036342"
      },
      {
          "url": "Print",
          "name": "Print",
          "category": "DocumentEditor",
          "order": 1,
          "component": "DocumentEditor",
          "dir": "DocumentEditor",
          "parentId": "035",
          "uid": "0036341"
      }],
      "order": 3,
      "uid": "035"
  },
  {
      "name": "ColorPicker",
      "directory": "ColorPicker",
      "category": "Editors",
      "type": "preview",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "ColorPicker",
            "uid": "0021210",
            "order": 0,
            "component": "ColorPicker",
            "dir": "ColorPicker",
            "parentId": "21"
        },
        {
            "url": "Inline",
            "name": "Inline Mode",
            "category": "ColorPicker",
            "uid": "0021211",
            "order": 0,
            "component": "ColorPicker",
            "dir": "ColorPicker",
            "parentId": "21"
        },
        {
            "url": "Custom",
            "name": "Custom Palettes",
            "category": "ColorPicker",
            "uid": "0021212",
            "order": 0,
            "component": "ColorPicker",
            "dir": "ColorPicker",
            "parentId": "21"
        },
        {
            "url": "Api",
            "name": "API",
            "category": "ColorPicker",
            "uid": "0021213",
            "order": 0,
            "component": "ColorPicker",
            "dir": "ColorPicker",
            "parentId": "21"
        }
      ],
      "order": 3,
      "uid": "21"
  },
  {
      "name": "RichTextEditor",
      "directory": "RichTextEditor",
      "category": "Editors",
      "type": "preview",
      "samples": [
          {
              "url": "Overview",
              "name": "Overview",
              "category": "RichTextEditor",
              "uid": "0035350",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "DefaultFunctionalities",
              "name": "Default Functionalities",
              "category": "RichTextEditor",
              "uid": "0035351",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Image",
              "name": "Image",
              "category": "RichTextEditor",
              "uid": "0035352",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Inline",
              "name": "Inline",
              "category": "RichTextEditor",
              "uid": "0035353",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "IFrame",
              "name": "IFrame",
              "category": "RichTextEditor",
              "uid": "0035354",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Print",
              "name": "Print",
              "category": "RichTextEditor",
              "uid": "0035355",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "AJAXContent",
              "name": "Ajax Content",
              "category": "RichTextEditor",
              "uid": "0035356",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "API",
              "name": "API",
              "category": "RichTextEditor",
              "uid": "0035357",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Events",
              "name": "Events",
              "category": "RichTextEditor",
              "uid": "0035358",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Forums",
              "name": "Use Case",
              "category": "RichTextEditor",
              "uid": "0035359",
              "order": 0,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "Types",
              "name": "Type",
              "category": "Toolbar",
              "uid": "0035360",
              "order": 1,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "CustomTool",
              "name": "Custom Tool",
              "category": "Toolbar",
              "uid": "0035361",
              "order": 1,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "DefaultMode",
              "name": "Overview",
              "category": "Markdown",
              "uid": "0035362",
              "order": 2,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          },
          {
              "url": "PreviewMode",
              "name": "Preview",
              "category": "Markdown",
              "uid": "0035363",
              "order": 2,
              "component": "RichTextEditor",
              "dir": "RichTextEditor",
              "parentId": "035"
          }
          // ,
          // {
          //     "url": "CustomFormat",
          //     "name": "Custom Format",
          //     "category": "Markdown",
          //     "uid": "0035364",
          //     "order": 2,
          //     "component": "RichTextEditor",
          //     "dir": "RichTextEditor",
          //     "parentId": "035"
          // }
      ],
      "order": 3,
      "uid": "035"
  },
  {
      "name": "ListView",
      "directory": "ListView",
      "category": "Layout",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "ListView",
            "uid": "0021210",
            "order": 0,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "RemoteData",
            "name": "Remote Data",
            "category": "ListView",
            "uid": "0021211",
            "order": 0,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "Checklist",
            "name": "Checklist",
            "category": "ListView",
            "uid": "0021212",
            "order": 0,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "NestedList",
            "name": "Nested List",
            "category": "ListView",
            "uid": "0021213",
            "order": 0,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "virtualization",
            "name": "Virtualization",
            "category": "ListView",
            "uid": "0021273",
            "order": 0,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021",
            "type": "new"
        },
        {
            "url": "Templates",
            "name": "Templates",
            "category": "Customization",
            "uid": "0021214",
            "order": 1,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "GroupTemplate",
            "name": "Group Template",
            "category": "Customization",
            "uid": "0021215",
            "order": 1,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        },
        {
            "url": "CallHistory",
            "name": "Call History",
            "category": "Use Case",
            "uid": "0021216",
            "order": 2,
            "component": "ListView",
            "dir": "ListView",
            "parentId": "021"
        }
      ],
      "order": 4,
      "uid": "021"
  },
  {
      "name": "Dialog",
      "directory": "Dialog",
      "category": "Layout",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Dialog",
            "uid": "0022220",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "CustomDialogs",
            "name": "Custom Dialogs",
            "category": "Dialog",
            "uid": "0022221",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "Modal",
            "name": "Modal",
            "category": "Dialog",
            "uid": "0022222",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "Dialog",
            "uid": "0022223",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022",
        },
        {
            "url": "AjaxContent",
            "name": "Ajax Content",
            "category": "Dialog",
            "uid": "0022224",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "Draggable",
            "name": "Draggable",
            "category": "Dialog",
            "uid": "0022225",
            "type": "new",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "Positioning",
            "name": "Positioning",
            "category": "Dialog",
            "uid": "0022226",
            "type": "new",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "Animation",
            "name": "Animation",
            "category": "Dialog",
            "uid": "0022227",
            "type": "new",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "MultipleDialogs",
            "name": "Multiple Dialogs",
            "category": "Dialog",
            "uid": "0022228",
            "type": "new",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "Dialog",
            "uid": "0022229",
            "order": 0,
            "component": "Dialog",
            "dir": "Dialog",
            "parentId": "022"
        }
      ],
      "order": 4,
      "uid": "022"
  },
  {
      "name": "Tooltip",
      "directory": "Tooltip",
      "category": "Layout",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Tooltip",
            "uid": "0023230",
            "order": 0,
            "component": "Tooltip",
            "dir": "Tooltip",
            "parentId": "023"
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "Tooltip",
            "uid": "0023231",
            "order": 0,
            "component": "Tooltip",
            "dir": "Tooltip",
            "parentId": "023"
        },
        {
            "url": "AjaxContent",
            "name": "Ajax Content",
            "category": "Tooltip",
            "uid": "0023232",
            "order": 0,
            "component": "Tooltip",
            "dir": "Tooltip",
            "parentId": "023"
        },
        {
            "url": "SmartPositioning",
            "name": "Smart Positioning",
            "category": "Tooltip",
            "uid": "0023233",
            "order": 0,
            "component": "Tooltip",
            "dir": "Tooltip",
            "parentId": "023"
        }
      ],
      "order": 4,
      "uid": "023"
  },
  {
      "name": "Card",
      "directory": "Card",
      "category": "Layout",
      "type": "update",
      "samples": [
        {
            "url": "BasicCard",
            "name": "Basic Card",
            "category": "Card",
            "uid": "0024240",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "VerticalCard",
            "name": "Vertical Card",
            "category": "Card",
            "uid": "0024241",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "HorizontalCard",
            "name": "Horizontal Card",
            "category": "Card",
            "uid": "0024242",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "SwipeableCard",
            "name": "Swipeable Card",
            "category": "Card",
            "uid": "0024243",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "FlipCard",
            "name": "Flip Card",
            "category": "Card",
            "uid": "0024244",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "RevealCard",
            "name": "Reveal Card",
            "category": "Card",
            "uid": "0024245",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        },
        {
            "url": "TileView",
            "name": "TileView",
            "category": "Card",
            "uid": "0024246",
            "order": 0,
            "component": "Card",
            "dir": "Card",
            "parentId": "024"
        }
      ],
      "order": 4,
      "uid": "024"
  },
  {
      "name": "Avatar",
      "directory": "Avatar",
      "category": "Layout",
      "type": "preview",
      "samples": [
        {
            "url": "Default",
            "name": "Default",
            "category": "Avatar",
            "uid": "0030309",
            "order": 0,
            "component": "Avatar",
            "dir": "Avatar",
            "parentId": "025"
        },
        {
            "url": "Types",
            "name": "Types",
            "category": "Avatar",
            "uid": "0030310",
            "order": 0,
            "component": "Avatar",
            "dir": "Avatar",
            "parentId": "025"
        },
        {
            "url": "Listview",
            "name": "ListView",
            "category": "Intergration",
            "uid": "0030311",
            "order": 1,
            "component": "Avatar",
            "dir": "Avatar",
            "parentId": "025"
        },
        {
            "url": "Badge",
            "name": "Badge",
            "category": "Intergration",
            "uid": "0030312",
            "order": 1,
            "component": "Avatar",
            "dir": "Avatar",
            "parentId": "025"
        },
        {
            "url": "Card",
            "name": "Card",
            "category": "Intergration",
            "uid": "0030313",
            "order": 1,
            "component": "Avatar",
            "dir": "Avatar",
            "parentId": "025"
        }
      ],
      "order": 4,
      "uid": "025"
  },

  {
      "name": "Badge",
      "directory": "Badge",
      "category": "Notifications",
      "type": "preview",
      "samples": [
        {
            "url": "Default",
            "name": "Default",
            "category": "Badge",
            "uid": "0030303",
            "order": 0,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
        {
            "url": "Types",
            "name": "Types",
            "category": "Badge",
            "uid": "0030304",
            "order": 0,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
        {
            "url": "Notification",
            "name": "Notification",
            "category": "Badge",
            "uid": "0030305",
            "order": 0,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
        {
            "url": "Listview",
            "name": "ListView",
            "category": "Integration",
            "uid": "0030306",
            "order": 1,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
        {
            "url": "Accordion",
            "name": "Accordion",
            "category": "Integration",
            "uid": "0030307",
            "order": 1,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
        {
            "url": "Toolbar",
            "name": "Toolbar",
            "category": "Integration",
            "uid": "0030308",
            "order": 1,
            "component": "Badge",
            "dir": "Badge",
            "parentId": "031"
        },
      ],
      "order": 0,
      "uid": "031"
  },
  {
      "name": "Sidebar",
      "directory": "Sidebar",
      "category": "Navigation",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Sidebar",
            "uid": "0025250",
            "order": 0,
            "component": "Sidebar",
            "dir": "Sidebar",
            "parentId": "025"
        },
        {
            "url": "API",
            "name": "API",
            "category": "Sidebar",
            "uid": "0025251",
            "order": 0,
            "component": "Sidebar",
            "dir": "Sidebar",
            "parentId": "025"
        },
        {
            "url": "Dock",
            "name": "Dock",
            "category": "Sidebar",
            "uid": "0025252",
            "order": 0,
            "component": "Sidebar",
            "dir": "Sidebar",
            "parentId": "025"
        },
        {
            "url": "SidebarWithListView",
            "name": "Sidebar With ListView",
            "category": "Sidebar",
            "uid": "0025253",
            "order": 0,
            "component": "Sidebar",
            "dir": "Sidebar",
            "parentId": "025"
        }
      ],
      "order": 5,
      "uid": "025"
  },
  {
      "name": "TreeView",
      "directory": "TreeView",
      "category": "Navigation",
      "type": "update",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "TreeView",
            "uid": "0026260",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "IconsandImages",
            "name": "Icons and Images",
            "category": "TreeView",
            "uid": "0026261",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "CheckBox",
            "name": "CheckBox",
            "category": "TreeView",
            "uid": "0026262",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "NodeEdit",
            "name": "Node Editing",
            "category": "TreeView",
            "uid": "0026263",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "MultiSelection",
            "name": "Multiple Selection",
            "category": "TreeView",
            "uid": "0026264",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "DragDrop",
            "name": "Drag and Drop",
            "category": "TreeView",
            "uid": "0026265",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026",
            "type": "update",
        },
        {
            "url": "Template",
            "name": "Template",
            "category": "TreeView",
            "uid": "0026266",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026",
            "type": "update",
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "TreeView",
            "uid": "0026267",
            "order": 0,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "LocalData",
            "name": "Local Data",
            "category": "Data Binding",
            "uid": "0026268",
            "order": 1,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        },
        {
            "url": "RemoteData",
            "name": "Remote Data",
            "category": "Data Binding",
            "uid": "0026269",
            "order": 1,
            "component": "TreeView",
            "dir": "TreeView",
            "parentId": "026"
        }
      ],
      "order": 5,
      "uid": "026"
  },
  {
      "name": "Tab",
      "directory": "Tab",
      "category": "Navigation",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Tab",
            "uid": "0027270",
            "order": 0,
            "component": "Tab",
            "dir": "Tab",
            "parentId": "027"
        },
        {
            "url": "Orientation",
            "name": "Orientation",
            "category": "Tab",
            "uid": "0027271",
            "order": 0,
            "component": "Tab",
            "dir": "Tab",
            "parentId": "027"
        },
        {
            "url": "ResponsiveModes",
            "name": "Responsive Modes",
            "category": "Tab",
            "uid": "0027272",
            "order": 0,
            "component": "Tab",
            "dir": "Tab",
            "parentId": "027"
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "Tab",
            "uid": "0027273",
            "order": 0,
            "component": "Tab",
            "dir": "Tab",
            "parentId": "027"
        },
        {
            "url": "Wizard",
            "name": "Wizard",
            "category": "Tab",
            "uid": "0027274",
            "order": 0,
            "component": "Tab",
            "dir": "Tab",
            "parentId": "027"
        }
      ],
      "order": 5,
      "uid": "027"
  },
  {
      "name": "Toolbar",
      "directory": "Toolbar",
      "category": "Navigation",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Toolbar",
            "uid": "0028280",
            "order": 0,
            "component": "Toolbar",
            "dir": "Toolbar",
            "parentId": "028"
        },
        {
            "url": "Popup",
            "name": "Popup",
            "category": "Toolbar",
            "uid": "0028281",
            "order": 0,
            "component": "Toolbar",
            "dir": "Toolbar",
            "parentId": "028"
        },
        {
            "url": "Alignment",
            "name": "Alignment",
            "category": "Toolbar",
            "uid": "0028282",
            "order": 0,
            "component": "Toolbar",
            "dir": "Toolbar",
            "parentId": "028"
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "Toolbar",
            "uid": "0028283",
            "order": 0,
            "component": "Toolbar",
            "dir": "Toolbar",
            "parentId": "028"
        }
      ],
      "order": 5,
      "uid": "028"
  },
  {
      "name": "ContextMenu",
      "directory": "ContextMenu",
      "category": "Navigation",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "DefaultFunctionalities",
            "category": "ContextMenu",
            "uid": "0029290",
            "order": 0,
            "component": "ContextMenu",
            "dir": "ContextMenu",
            "parentId": "029"
        }
      ],
      "order": 5,
      "uid": "029"
  },
  {
      "name": "Accordion",
      "directory": "Accordion",
      "category": "Navigation",
      "samples": [
        {
            "url": "DefaultFunctionalities",
            "name": "Default Functionalities",
            "category": "Accordion",
            "uid": "0030300",
            "order": 0,
            "component": "Accordion",
            "dir": "Accordion",
            "parentId": "030"
        },
        {
            "url": "Ajax",
            "name": "Ajax Content",
            "category": "Accordion",
            "uid": "0030301",
            "order": 0,
            "component": "Accordion",
            "dir": "Accordion",
            "parentId": "030"
        },
        {
            "url": "Icons",
            "name": "Icons",
            "category": "Accordion",
            "uid": "0030302",
            "order": 0,
            "component": "Accordion",
            "dir": "Accordion",
            "parentId": "030"
        },
        {
            "url": "RTL",
            "name": "RTL",
            "category": "Accordion",
            "uid": "0030303",
            "order": 0,
            "component": "Accordion",
            "dir": "Accordion",
            "parentId": "030"
        }
      ],
      "order": 5,
      "uid": "030"
  },
  {
      "name": "XlsIO",
      "directory": "XlsIO",
      "category": "File Formats",
      "type": "update",
      "samples": [{
          "url": "Create",
          "name": "Create",
          "category": "Getting Started",
          "order": 0,
          "component": "XlsIO",
          "dir": "XlsIO",
          "uid": "0031310",
          "parentId": "031"
      },
          {
              "url": "FormatCells",
              "name": "Format Cells",
              "category": "Formatting",
              "order": 1,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031311",
              "parentId": "031"
          },
          {
              "url": "ConditionalFormatting",
              "name": "Conditional Formatting",
              "category": "Formatting",
              "order": 1,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031312",
              "parentId": "031"
          },
          {
              "url": "ChartWorksheet",
              "name": "Chart Worksheet",
              "category": "Charts",
              "order": 2,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031313",
              "parentId": "031"
          },
          {
              "url": "EmbeddedChart",
              "name": "Embedded Chart",
              "category": "Charts",
              "order": 2,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031314",
              "parentId": "031"
          },
          {
              "url": "Sparklines",
              "name": "Sparklines",
              "category": "Charts",
              "order": 2,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031315",
              "parentId": "031"
          },
          {
              "url": "Formulas",
              "name": "Formulas",
              "category": "Formulas",
              "order": 3,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031316",
              "parentId": "031"
          },
          {
              "url": "DataValidation",
              "name": "Data Validation",
              "category": "Data Management",
              "order": 4,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031317",
              "parentId": "031"
          },
          {
              "url": "DataSorting",
              "name": "Data Sorting",
              "category": "Data Management",
              "order": 4,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031318",
              "parentId": "031"
          },
          {
              "url": "FormControls",
              "name": "Form Controls",
              "category": "Data Management",
              "order": 4,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "0031319",
              "parentId": "031"
          },
          {
              "url": "AutoFilter",
              "name": "Filters",
              "category": "Data Management",
              "order": 4,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313110",
              "parentId": "031"
          },
          {
              "url": "BusinessObjects",
              "name": "Business Objects",
              "category": "Data Binding",
              "order": 5,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313111",
              "parentId": "031"
          },
          {
              "url": "ExcelToPDF",
              "name": "Excel To PDF",
              "category": "Export",
              "order": 6,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313112",
              "parentId": "031"
          },
          {
              "url": "EncryptAndDecrypt",
              "name": "Encrypt and Decrypt",
              "category": "Settings",
              "order": 7,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313113",
              "parentId": "031"
          },
          {
              "url": "Tables",
              "name": "Tables",
              "category": "Business Intelligence",
              "order": 8,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313114",
              "parentId": "031"
          },
          {
              "url": "AutoShapes",
              "name": "AutoShapes",
              "category": "Shapes",
              "order": 9,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313115",
              "parentId": "031"
          },
          {
              "url": "GroupShapes",
              "name": "Group Shapes",
              "category": "Shapes",
              "type": "new",
              "order": 9,
              "component": "XlsIO",
              "dir": "XlsIO",
              "uid": "00313116",
              "parentId": "031"
          },
      ],
      "order": 6,
      "uid": "031"
  },
  {
      "name": "Presentation",
      "directory": "Presentation",
      "category": "File Formats",
      "type": "update",
      "samples": [{
          "url": "Default",
          "name": "Create Presentation",
          "category": "Getting Started",
          "order": 0,
          "component": "Presentation",
          "dir": "Presentation",
          "parentId": "032",
          "uid": "0032320"
      },
          {
              "url": "Chart",
              "name": "Chart",
              "category": "Slide Elements",
              "order": 1,
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032321"
          },
          {
              "url": "Slide",
              "name": "Slide",
              "category": "Slide Elements",
              "order": 1,
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032322"
          },
          {
              "url": "SmartArt",
              "name": "SmartArt",
              "category": "Slide Elements",
              "order": 1,
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032323"
          },
          {
              "url": "Table",
              "name": "Table",
              "category": "Slide Elements",
              "order": 1,
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032324"
          },
          {
              "url": "Comment",
              "name": "Comment",
              "category": "Slide Elements",
              "order": 1,
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032325"
          },
          {
              "url": "CreateAnimation",
              "name": "Create Animation",
              "category": "Slide Elements",
              "order": 1,
              "type": "new",
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032326"
          },
          {
              "url": "ModifyAnimation",
              "name": "Modify Animation",
              "category": "Slide Elements",
              "order": 1,
              "type": "new",
              "component": "Presentation",
              "dir": "Presentation",
              "parentId": "032",
              "uid": "0032327"
          }
      ],
      "order": 7,
      "uid": "032"
  },
  {
      "name": "PDF",
      "directory": "PDF",
      "category": "File Formats",
      "type": "update",
      "samples": [
          {
              "url": "Default",
              "name": "Default",
              "category": "PDF",
              "order": 0,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032320"
          },
          {
              "url": "JobApplication",
              "name": "Job Application",
              "category": "Product Showcase",
              "order": 1,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032321"
          },
          {
              "url": "HelloWorld",
              "name": "Hello World",
              "category": "Getting Started",
              "order": 2,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032322"
          },
          {
              "url": "TextFlow",
              "name": "Text Flow",
              "category": "Drawing Text",
              "order": 3,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032323"
          },
          {
              "url": "BulletsandLists",
              "name": "Bullets and Lists",
              "category": "Drawing Text",
              "order": 3,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032324"
          },
          {
              "url": "RTLSupport",
              "name": "RTL Text",
              "category": "Drawing Text",
              "order": 3,
              "type": "new",
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032326"
          },
          {
              "url": "Barcode",
              "name": "Barcode",
              "category": "Graphics",
              "order": 4,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032327"
          },
          {
              "url": "DrawingShapes",
              "name": "Drawing Shapes",
              "category": "Graphics",
              "order": 4,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032328"
          },
          {
              "url": "GraphicBrushes",
              "name": "Graphic Brushes",
              "category": "Graphics",
              "order": 4,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "0032329"
          },
          {
              "url": "ImageInsertion",
              "name": "Image Insertion",
              "category": "Graphics",
              "order": 4,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323210"
          },
          {
              "url": "AdventureCycle",
              "name": "Adventure Cycle Works",
              "category": "Tables",
              "order": 5,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00313211"
          },
          {
              "url": "TableFeatures",
              "name": "Table Features",
              "category": "Tables",
              "order": 5,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00313212"
          },
          {
              "url": "ExtractText",
              "name": "Text Extraction",
              "category": "Import and Export",
              "order": 6,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323213"
          },
          {
              "url": "FindText",
              "name": "Find Text",
              "category": "Import and Export",
              "order": 6,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323214"
          },
          {
              "url": "DocToPDF",
              "name": "Doc To PDF",
              "category": "Import and Export",
              "order": 6,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323215"
          },
          {
              "url": "Encryption",
              "name": "Encryption",
              "category": "Security",
              "order": 7,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323216"
          },
          {
              "url": "DigitalSignature",
              "name": "Digital Signature",
              "category": "Security",
              "order": 7,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323217"
          },
          {
              "url": "DocumentSettings",
              "name": "Document Settings",
              "category": "Settings",
              "order": 8,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323218"
          },
          {
              "url": "PageSettings",
              "name": "Page Settings",
              "category": "Settings",
              "order": 8,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323219"
          },
          {
              "url": "HeadersandFooters",
              "name": "Headers and Footers",
              "category": "Settings",
              "order": 8,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323220"
          },
          {
              "url": "Layers",
              "name": "Layers",
              "category": "Settings",
              "order": 8,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323221"
          },
          {
              "url": "InteractiveFeatures",
              "name": "Interactive Features",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323222"
          },
          {
              "url": "FormFilling",
              "name": "Form Filling",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323223"
          },
          {
              "url": "Portfolio",
              "name": "Portfolio",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323224"
          },
          {
              "url": "AnnotationFlatten",
              "name": "Annotations",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323225"
          },
          {
              "url": "NamedDestination",
              "name": "Named Destination",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323226"
          },
          {
              "url": "Bookmarks",
              "name": "Bookmarks",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323227"
          },
          {
              "url": "Attachments",
              "name": "File Attachments",
              "category": "User Interaction",
              "order": 9,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323228"
          },
          {
              "url": "MergeDocuments",
              "name": "Merge Documents",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323229"
          },
          {
              "url": "SplitPDF",
              "name": "Split PDF",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323230"
          },
          {
              "url": "OverlayDocuments",
              "name": "Overlay Documents",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "031",
              "uid": "00323231"
          },
          {
              "url": "Booklet",
              "name": "Booklet",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323232"
          },
          {
              "url": "ImportandStamp",
              "name": "Import and Stamp",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323233"
          },
          {
              "url": "RearrangePages",
              "name": "Rearrange Pages",
              "category": "Modify Documents",
              "order": 10,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323234"
          },
          {
              "url": "Autotag",
              "name": "Autotag",
              "category": "Accessibility",
              "order": 11,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323235"
          },
          {
              "url": "Customtag",
              "name": "Customtag",
              "category": "Accessibility",
              "order": 11,
              "component": "PDF",
              "dir": "PDF",
              "parentId": "032",
              "uid": "00323236"
          }
      ],
      "order": 8,
      "uid": "033"
  },
    {
        "name": "DocIO",
        "directory": "DocIO",
        "category": "File Formats",
        "type": "update",
        "samples": [{
            "url": "SalesInvoice",
            "name": "Sales Invoice",
            "category": "Product Showcase",
            "order": 0,
            "component": "DocIO",
            "dir": "DocIO",
            "parentId": "034",
            "uid": "0034340"
        },
            {
                "url": "UpdateFields",
                "name": "Update Fields",
                "category": "Product Showcase",
                "order": 0,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034341"
            },
            {
                "url": "HelloWorld",
                "name": "Hello World",
                "category": "Getting Started",
                "order": 1,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034342"
            },
            {
                "url": "FindandHighlight",
                "name": "Find and Highlight",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034343"
            },
            {
                "url": "SimpleReplace",
                "name": "Simple Replace",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034344"
            },
            {
                "url": "AdvancedReplace",
                "name": "Advanced Replace",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034345"
            },
            {
                "url": "BookmarkNavigation",
                "name": "Bookmark Navigation",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034346"
            },
            {
                "url": "CloneandMerge",
                "name": "Clone and Merge",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034347"
            },
            {
                "url": "Forms",
                "name": "Forms",
                "category": "Editing",
                "order": 2,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034348"
            },
            {
                "url": "FormFillingAndProtection",
                "name": "Form filling and Protection",
                "category": "Content Control",
                "order": 3,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "0034349"
            },
            {
                "url": "XMLMapping",
                "name": "XML Mapping",
                "category": "Content Control",
                "order": 3,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343410"
            },
            {
                "url": "Bookmarks",
                "name": "Bookmarks",
                "category": "Insert Content",
                "order": 4,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343411"
            },
            {
                "url": "HeaderandFooter",
                "name": "Header and Footer",
                "category": "Insert Content",
                "order": 4,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343412"
            },
            {
                "url": "ImageInsertion",
                "name": "Image Insertion",
                "category": "Insert Content",
                "order": 4,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343413"
            },
            {
                "url": "InsertOLEObject",
                "name": "Insert OLE Object",
                "category": "Insert Content",
                "order": 4,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343414"
            },
            {
                "url": "FormatTable",
                "name": "Format Table",
                "category": "Formatting",
                "order": 5,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343415"
            },
            {
                "url": "RTL",
                "name": "RTL",
                "category": "Formatting",
                "order": 5,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343416"
            },
            {
                "url": "Styles",
                "name": "Styles",
                "category": "Formatting",
                "order": 5,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343417"
            },
            {
                "url": "TableStyles",
                "name": "Table Styles",
                "category": "Formatting",
                "order": 5,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343418"
            },
            {
                "url": "EmployeeReport",
                "name": "Employee Report",
                "category": "Mail Merge",
                "order": 6,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343419"
            },
            {
                "url": "MailMergeEvent",
                "name": "Mail Merge Event",
                "category": "Mail Merge",
                "order": 6,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343420"
            },
            {
                "url": "NestedMailMerge",
                "name": "Nested Mail Merge",
                "category": "Mail Merge",
                "order": 6,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343421"
            },
            {
                "url": "WordToPDF",
                "name": "Word to PDF",
                "category": "Import and Export",
                "order": 7,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343422",
                "type": "update"
            },
            {
                "url": "WordToWordML",
                "name": "Word to WordML",
                "category": "Import and Export",
                "order": 7,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343423"
            },
            {
                "url": "WordMLToWord",
                "name": "WordML to Word",
                "category": "Import and Export",
                "order": 7,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343424"
            },
            {
                "url": "RTFToDoc",
                "name": "RTF to Word",
                "category": "Import and Export",
                "order": 7,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343425"
            },
            {
                "url": "DOCtoODT",
                "name": "Word to ODT",
                "category": "Import and Export",
                "order": 7,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343426"
            },
            {
                "url": "InsertBreak",
                "name": "Insert Break",
                "category": "Page Layout",
                "order": 8,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343427"
            },
            {
                "url": "Watermark",
                "name": "Watermark",
                "category": "Page Layout",
                "order": 8,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343428"
            },
            {
                "url": "FootnotesandEndnotes",
                "name": "Footnotes and Endnotes",
                "category": "References",
                "order": 9,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343429"
            },
            {
                "url": "AutoShapes",
                "name": "AutoShapes",
                "category": "Shapes",
                "order": 10,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343430"
            },
            {
                "url": "PieChart",
                "name": "Pie Chart",
                "category": "Charts",
                "order": 11,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343431"
            },
            {
                "url": "BarChart",
                "name": "Bar Chart",
                "category": "Charts",
                "order": 11,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343432"
            },
            {
                "url": "DocumentSettings",
                "name": "Document Settings",
                "category": "View",
                "order": 12,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343433"
            },
            {
                "url": "MacroPreservation",
                "name": "Macro Preservation",
                "category": "View",
                "order": 12,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343434"
            },
            {
                "url": "DocumentProtection",
                "name": "Document Protection",
                "category": "Security",
                "order": 13,
                "component": "DocIO",
                "dir": "DocIO",
                "parentId": "034",
                "uid": "00343435"
            }
        ],
        "order": 9,
        "uid": "034"
    },
    {
        "name": "Toast",
        "directory": "Toast",
        "category": "Notifications",
        "type": "preview",
        "samples": [
            {
                "url": "DefaultFunctionalities",
                "name": "Default Functionalities",
                "category": "Toast",
                "uid": "00343436",
                "order": 0,
                "component": "Toast",
                "dir": "Toast",
                "parentId": "035"
            },
            {
                "url": "Types",
                "name": "Types",
                "category": "Toast",
                "uid": "00343437",
                "order": 0,
                "component": "Toast",
                "dir": "Toast",
                "parentId": "035"
            },
            {
                "url": "Template",
                "name": "Template",
                "category": "Toast",
                "uid": "00343438",
                "order": 0,
                "component": "Toast",
                "dir": "Toast",
                "parentId": "035"
            },
            {
                "url": "Position",
                "name": "Position",
                "category": "Toast",
                "uid": "00343439",
                "order": 0,
                "component": "Toast",
                "dir": "Toast",
                "parentId": "035"
            },
            {
                "url": "API",
                "name": "API",
                "category": "Toast",
                "uid": "00343440",
                "order": 0,
                "component": "Toast",
                "dir": "Toast",
                "parentId": "035"
            },
        ],
        "order": 10,
        "uid": "035"
    }
]