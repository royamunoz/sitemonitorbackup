﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SiteMonitor.Domain.Models;
using SiteMonitor.Services;
using SiteMonitor.Web.Controllers.Resources;

namespace SiteMonitor.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
#if DEBUG
#else
        [Authorize(AuthenticationSchemes = "Bearer")]
#endif
    public class ApiController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISiteService _siteService;
        private readonly ILicenceService _licenceService;
        private readonly IPriceChangeService _priceChangeService;

        public ApiController(
            IMapper mapper,
            ISiteService siteService,
            ILicenceService licenceService,
            IPriceChangeService priceChangeService
            )
        {
            _mapper = mapper;
            _siteService = siteService;
            _licenceService = licenceService;
            _priceChangeService = priceChangeService;
        }

        /// <summary>
        /// ok
        /// http://localhost:55474/api/updatelicence
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatelicence")]
        public async Task<IActionResult> UpdateLicence([FromBody] UpdateLicenceResource data)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //check if id send by API exists in the db. 
            var site = await _siteService.GetSiteAsync(data.SiteNumber);
            if (site == null)
                return NotFound();

            var lu = _mapper.Map<LicenceState>(data);
            lu.SiteId = site.Id;

            await _licenceService.UpdateLicence(lu);

            return Ok();
        }

        /// <summary>
        /// incomplete because method do not exist in Services of price change.
        /// http://localhost:55474/api/updatepricechange
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatepricechange")]
        public async Task<IActionResult> UpdatePriceChange([FromBody] PriceChangeUpdateResource data)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var site = await _siteService.GetSiteAsync(data.SiteNumber);
            if (site == null)
                return NotFound();

            var pc = _mapper.Map<PriceChangeUpdate>(data);
            pc.SiteId = site.Id;

            _priceChangeService.UpdatePriceChange(pc);

            return Ok();
        }

        /// <summary>
        /// OK
        /// http://localhost:55474/api/pendingchanges?SiteNumber=1&LatestChange=828
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("pendingchanges")]
        public async Task<IActionResult> PendingChanges([FromQuery] PendingPriceChangeCheckResource data)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var site = await _siteService.GetSiteAsync(data.SiteNumber);
            if (site == null)
                return NotFound();
            
            var changes = _mapper.Map<List<PendingPriceChangeResource>>(_priceChangeService.GetPendingPriceChanges(site.Id, data.LatestChange));
            return Ok(changes);
        }

        /// <summary>
        /// should return every price change that is pending, even if the last price change is pending
        /// ok
        /// http://localhost:55474/api/pendingpricechange
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("pendingpricechange")]
        public async Task<IActionResult> PendingPriceChange([FromBody] LastPriceChangeResource  data)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var site = await _siteService.GetSiteAsync(data.SiteNumber);
            if (site == null)
                return NotFound();

            var pendingPriceChange = _priceChangeService.GetPendingPriceChanges(site.Id, data.LatestPriceChangeId);
            return Ok(pendingPriceChange);
            
        }

    }
}