﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public class DashboardData
    {
        [Display(Name = "Total De Estaciones")]
        public int SiteCount { get; set; }
        [Display(Name = "Licencias Críticas")]
        public int CriticalLicenceCount { get; set; }
        [Display(Name = "Licencias en Alerta")]
        public int WarningLicenceCount { get; set; }
        [Display(Name = "Licencias Normales")]
        public int OkLicenceCount { get; set; }
        [Display(Name = "Estaciones Con Precios Programados")]
        public int SitesWithScheduledPricesCount { get; set; }
        [Display(Name = "Estaciones Sin Precios Programados")]
        public int SitesWithoutScheduledPricesCount { get; set; }

        public List<PriceChange> PriceChanges { get; set; }

        public Dictionary<string, string> dictionaryChart1 { get; set; }
        public Dictionary<string, string> dictionaryChart2 { get; set; }

        public DashboardData()
        {
            PriceChanges = new List<PriceChange>();
        }
    }
}
