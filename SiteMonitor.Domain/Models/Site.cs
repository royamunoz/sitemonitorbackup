﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Domain.Models
{
    public class Site
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Número")]
        public int Number { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        [Display(Name = "Activo")]
        public bool IsActive { get; set; }

        [Display(Name = "Programar cambios de precio")]
        public bool ReceivesPriceChanges { get; set; }

        //public ICollection<PriceChangeState> ScheduledPriceChanges { get; set; }
        public ICollection<LicenceState> LicenceStates { get; set; }

    }
}
