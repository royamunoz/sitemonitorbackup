﻿using System;
using System.Linq;
using SiteMonitor.Data.Helpers;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public static class DbInitializer
    {

        public static void Seed(AppDbContext context)

        {
            SeedSites(context);

            SeedGrades(context);

            ResetDemoLicenceStates(context);

            ResetDemoPriceChangeStates(context);

            ResetDemoScheduledPrices(context);
        }

        private static void ResetDemoScheduledPrices(AppDbContext context)
        {
            var pricechanges = context.PriceChanges;
            context.PriceChanges.RemoveRange(pricechanges);
            context.SaveChanges();

            var now = DateTime.Now;

            var grades = context.Grades.ToList();
            foreach (var grade in grades)
            {
                PriceChange p = new PriceChange
                {
                    GradeId = grade.Id,
                    ScheduledBy = "TestUser",
                    Price = grade.Id* 100,
                    ScheduledOn = now.AddHours(-2),
                    ChangesOn = now,
                    IsPending = true
                };
                context.PriceChanges.Add(p);
            }

            context.SaveChanges();

        }

        private static void ResetDemoPriceChangeStates(AppDbContext context)
        {
            var priceStates = context.PriceChangeStates;
            context.PriceChangeStates.RemoveRange(priceStates);
            context.SaveChanges();

            var sites = context.Sites.Where(s=>s.IsActive == true);
            foreach (var site in sites)
                context.PriceChangeStates.AddRange(PriceChangeGenerator.GetRandomPriceStateForSite(site.Id));

            context.SaveChanges();
        }

        private static void SeedGrades(AppDbContext context)
        {
            if (!context.Grades.Any())
            {
                context.AddRange(
                    new Grade { Id = 1, Name = "Super" },
                    new Grade { Id = 2, Name = "Plus 91" },
                    new Grade { Id = 3, Name = "Diesel" },
                    new Grade { Id = 4, Name = "GLP" }
                );
                context.SaveChanges();
            }
        }

        private static void SeedSites(AppDbContext context)
        {
            if (!context.Sites.Any())
            {
                context.AddRange
                (
                    new Site { Number = 1, Name = "Cerro Agrícola", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 2, Name = "Uno - Lindora", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 3, Name = "Coopetico", IsActive = false, ReceivesPriceChanges = true },
                    new Site { Number = 4, Name = "Uno - Santa Ana", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 5, Name = "Coopeheredia", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 6, Name = "Sardinal", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 7, Name = "Coopetaxi", IsActive = false, ReceivesPriceChanges = true },
                    new Site { Number = 8, Name = "Alagas", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 9, Name = "Dynamo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 10, Name = "La Galera", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 11, Name = "Los Angeles", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 12, Name = "El Labrador", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 13, Name = "CJB - Naranjo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 14, Name = "JSM - Interamericana", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 15, Name = "JSM - CICSA", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 16, Name = "CJB - Super Naranjo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 17, Name = "JSM - Sercopa", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 18, Name = "G24", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 19, Name = "JSM - La Tropicana", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 20, Name = "San Pedro", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 21, Name = "Alymo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 22, Name = "Super Barato", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 23, Name = "El Ceibo", IsActive = false, ReceivesPriceChanges = true },
                    new Site { Number = 24, Name = "JSM - Turrialba", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 25, Name = "JSM - San Ramon", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 26, Name = "Zavillana - Santa Eduviges", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 27, Name = "JSM - Serviexpo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 28, Name = "Servicentro Ecologico San Jorge", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 29, Name = "Casaque", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 30, Name = "JSM - Costa Rica", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 31, Name = "JSM - Santa Ana", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 32, Name = "Santa Teresita", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 33, Name = "JSM - Alajuelita", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 34, Name = "La Fortuna", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 35, Name = "JSM - La Suiza", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 36, Name = "Panamericana", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 37, Name = "JSM - Tamarindo", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 38, Name = "JSM - Santa Cruz", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 39, Name = "La Cristalina", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 40, Name = "JSM - El Liberiano", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 41, Name = "JSM - Liberia", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 42, Name = "JSM - Puerto Viejo", IsActive = false, ReceivesPriceChanges = true },
                    new Site { Number = 43, Name = "JSM - Cañas", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 44, Name = "JSM - Tilaran", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 45, Name = "MiGas", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 46, Name = "Barrio Cuba", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 47, Name = "JSM - Nicoya", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 48, Name = "JSM Filadelfia", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 49, Name = "Carrizal", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 50, Name = "Rio Grande", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 51, Name = "Servicentro Nicoya", IsActive = true, ReceivesPriceChanges = true },
                    new Site { Number = 52, Name = "Servicentro del Oeste", IsActive = true, ReceivesPriceChanges = true }
                );

                context.SaveChanges();
            }
        }

        private static void ResetDemoLicenceStates(AppDbContext context)
        {
            var states = context.LicenceStates;
            context.LicenceStates.RemoveRange(states);

            context.SaveChanges();

            var sites = context.Sites.Where(s => s.IsActive == true); ;
            foreach (var site in sites)
            {
                LicenceState ls = new LicenceState
                {
                    SiteId = site.Id,
                    ExpirationDate = DateGenerator.ExpirationDate(31),
                    UpdatedOn = DateGenerator.UpdatedOnDate(24)
                };
                context.LicenceStates.Add(ls);
            }
            context.SaveChanges();
        }
    }
}
