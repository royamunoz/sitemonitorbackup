﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Services
{
    public class PriceChangeData
    {
        [Display(Name = "Id")]
        public int GradeId { get; set; }
        [Display(Name = "Combustible")]
        public string GradeName { get; set; }
        [Display(Name = "Precio")]
        public decimal Price { get; set; }
        [Display(Name = "Fecha De Cambio")]
        public DateTime ChangesOn { get; set; }
        public DateTime ScheduledOn { get; set; }
        public DateTime UpdatedOn { get; set; }

    }
}