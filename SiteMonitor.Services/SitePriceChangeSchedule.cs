﻿using System.Collections.Generic;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public class SitePriceChangeSchedule
    {
        public Site Site { get; set; }
        public List<PriceChangeData> Schedule { get; set; }

        public SitePriceChangeSchedule()
        {
            Schedule = new List<PriceChangeData>();
        }
    }
}
