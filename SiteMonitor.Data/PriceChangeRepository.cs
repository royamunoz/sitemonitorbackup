﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class PriceChangeRepository : IPriceChangeRepository
    {

        private readonly AppDbContext _appDbContext;

        public PriceChangeRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<PriceChangeState> GetScheduledPricesForAllSites(bool includeGradeInfo = false)
        {
            var query = _appDbContext.PriceChangeStates;
            if (includeGradeInfo)
                query.Include(p => p.Grade);

            return query;
            //var query = _appDbContext.PriceChangeStates;
            //if (includeGradeInfo)
            //    query.Include(p => p.Grade).Include(p => p.Site.Name);

            //var dto = query.Select(x => new PriceChangeState { Grade = x.Grade, Price = x.Price, Id = x.Id });

            //return dto;
        }

        public IEnumerable<PriceChangeState> GetJose(bool includeGradeInfo = false)
        {
            var query = _appDbContext.PriceChangeStates;
            if (includeGradeInfo)
                query.Include(p => p.Grade).Include(p => p.Site.Name);

            var dto = query.Select(x => new PriceChangeState { Grade = x.Grade , Price = x.Price , Id = x.Id });

            return dto;
        }

        public IEnumerable<PriceChangeState> GetScheduledPricesForSite(int siteId, bool includeGradeInfo = false)
        {
            var query = _appDbContext.PriceChangeStates;
            if (includeGradeInfo)
            {
                var  ret= query.Where(s => s.SiteId == siteId).Include(p => p.Grade);
                var final = ret.Select(x => new PriceChangeState { Grade = x.Grade, Price = x.Price, Id = x.Id });
                return final;
            }

            return _appDbContext.PriceChangeStates.Where(s => s.SiteId == siteId);
             
             
           /* var query = _appDbContext.PriceChangeStates;
            if (includeGradeInfo)
                query.Where(s => s.SiteId == siteId).Include(p => p.Grade);

            var dto = query.Select(x => new PriceChangeState { Grade = x.Grade, Price = x.Price, Id = x.Id });

            return dto;*/

            //return _appDbContext.PriceChangeStates.Where(s => s.SiteId == siteId);
        }

        public IEnumerable<PriceChange> GetAllPendingPriceChange(bool includeGradeInfo = false)
        {
            if (includeGradeInfo)
                return _appDbContext.PriceChanges.Where(p => p.IsPending == true).Include(p => p.Grade);

            return _appDbContext.PriceChanges.Where(p => p.IsPending == true);
        }

        public IEnumerable<PriceChange> GetAllPendingPriceChange(int latestPriceChangeId, bool includeGradeInfo = false)
        {
            if (includeGradeInfo)
                return _appDbContext.PriceChanges.Where(p => p.Id > latestPriceChangeId && p.IsPending == true).Include(p => p.Grade);

            return _appDbContext.PriceChanges.Where(p => p.Id >= latestPriceChangeId && p.IsPending == true);
        }



        public void AddPriceChange(PriceChange priceChange)
        {
            _appDbContext.PriceChanges.Add(priceChange);
            _appDbContext.SaveChanges();
        }

        public void AddPriceChanges(IEnumerable<PriceChange> prices)
        {
            _appDbContext.PriceChanges.AddRange(prices);
            _appDbContext.SaveChanges();

        }

        public IEnumerable<Grade> GetGrades()
        {
            return _appDbContext.Grades;
        }

        public PriceChange GetPriceChange(int priceChangeId)
        {
            return _appDbContext.PriceChanges.FirstOrDefault(p => p.Id == priceChangeId);
        }

        public PriceChangeState GetPriceChangeState(int PriceChangeStateId)
        {
            return _appDbContext.PriceChangeStates.FirstOrDefault(p => p.Id == PriceChangeStateId);
        }

        
    }
}
