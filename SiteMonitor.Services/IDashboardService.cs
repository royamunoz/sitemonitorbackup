﻿namespace SiteMonitor.Services.Services
{
    public interface IDashboardService
    {
        DashboardData GetDashboardData();
    }
}