﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public interface ILicenceRepository
    {
        IEnumerable<LicenceState> GetLicenceStates(bool includeSiteInfo);
        Task UpdateLicence(LicenceState state);
    }
}