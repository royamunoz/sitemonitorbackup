﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public interface ISiteRepository
    {
        IEnumerable<Site> GetActiveSites(bool onlyActiveSites);
        Task<Site> GetSiteAsync(int siteId);
        Site GetSiteById(int id);
        Task UpdateSite(Site site);
    }
}