﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class UpdateLicenceResource
    {
        [Required]
        public int SiteNumber { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
    }
}
