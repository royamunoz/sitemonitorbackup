﻿using System.Collections.Generic;

namespace SiteMonitor.Domain.Models
{
    public class PriceChangeUpdate
    {
        public Site Site { get; set; }
        public int SiteId { get; set; }

        public IEnumerable<PriceChange> Schedule { get; set; }

        public PriceChangeUpdate()
        {
            Schedule = new List<PriceChange>();
        }
    }
}
