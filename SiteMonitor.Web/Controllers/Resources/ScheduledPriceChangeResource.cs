﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class ScheduledPriceChangeResource
    {
        public int Id { get; set; }
        public List<PriceChangeResource> Schedule { get; set; }

        public ScheduledPriceChangeResource()
        {
            Schedule = new List<PriceChangeResource>();
        }
    }
}
