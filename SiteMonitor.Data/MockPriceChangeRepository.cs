﻿using System;
using System.Collections.Generic;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class MockPriceChangeRepository : IPriceChangeRepository
    {
        public IEnumerable<PriceChangeState> GetScheduledPricesForAllSites(bool includeGradeInfo = false)
        {
            List<PriceChangeState> states = new List<PriceChangeState>();
            for(int i=0;i<50;i++)
                states.AddRange(GetRandomPriceChangeStatesForSite(i+1));

            return states;
        }

        public IEnumerable<PriceChangeState> GetScheduledPricesForSite(int siteId, bool includeGradeInfo=false)
        {
            return GetRandomPriceChangeStatesForSite(siteId);
        }


        public IEnumerable<PriceChange> GetAllPendingPriceChange(bool includeGradeInfo)
        {
            List<PriceChange> priceChanges = new List<PriceChange>();
            for (int i = 0; i < 50; i++)
                priceChanges.AddRange(GetRandomPriceChanges(i + 1));

            return priceChanges;
        }

        public void AddPriceChange(PriceChange priceChange) {

        }

        public void AddPriceChanges(IEnumerable<PriceChange> priceChange)
        {

        }

        public IEnumerable<Grade> GetGrades()
        {
            Grade super = new Grade() { Id = 1, Name = "Super" };
            Grade plus91 = new Grade() { Id = 2, Name = "Plus91" };
            Grade diesel = new Grade() { Id = 3, Name = "Diesel" };
            Grade lpg = new Grade() { Id = 4, Name = "LPG" };

            return new List<Grade>{super,plus91,diesel,lpg};
        }

        private List<PriceChangeState> GetRandomPriceChangeStatesForSite(int id)
        {
            DateTime now = DateTime.Now;

            Site site = new Site() { Id = id,IsActive = true,Name = $"Site {id}"};

            Grade super = new Grade() {Id = 1, Name = "Super"};
            Grade plus91 = new Grade() { Id = 2, Name = "Plus91" };
            Grade diesel = new Grade() { Id = 3, Name = "Diesel" };
            Grade lpg = new Grade() { Id = 4, Name = "LPG" };

            PriceChangeState psSuper = new PriceChangeState()
            {
                Id = id,
                Site = site,
                SiteId = site.Id,
                Grade = super,
                GradeId = super.Id,
                Price = 600,
                ChangesOn = now,
                ScheduledOn = now,
                UpdatedOn = now
            };

            PriceChangeState psPlus91 = new PriceChangeState()
            {
                Id = id,
                Site = site,
                SiteId = site.Id,
                Grade = plus91,
                GradeId = plus91.Id,
                Price = 550,
                ChangesOn = now,
                ScheduledOn = now,
                UpdatedOn = now
            };

            PriceChangeState psDiesel = new PriceChangeState()
            {
                Id = id,
                Site = site,
                SiteId = site.Id,
                Grade = diesel,
                GradeId = diesel.Id,
                Price = 500,
                ChangesOn = now,
                ScheduledOn = now,
                UpdatedOn = now
            };

            PriceChangeState psLpg = new PriceChangeState()
            {
                Id = id,
                Site = site,
                SiteId = site.Id,
                Grade = lpg,
                GradeId = lpg.Id,
                Price = 400,
                ChangesOn = now,
                ScheduledOn = now,
                UpdatedOn = now
            };

            return new List<PriceChangeState>(){psSuper,psPlus91,psDiesel,psLpg};

        }

        private List<PriceChange> GetRandomPriceChanges(int id)
        {
            DateTime now = DateTime.Now;

            Grade super = new Grade() { Id = 1, Name = "Super" };
            Grade plus91 = new Grade() { Id = 2, Name = "Plus91" };
            Grade diesel = new Grade() { Id = 3, Name = "Diesel" };
            Grade lpg = new Grade() { Id = 4, Name = "LPG" };

            PriceChange pcSuper = new PriceChange()
            {
                Id = id,
                ChangesOn = now,
                Grade = super,
                GradeId = super.Id,
                IsPending = true,
                Price = 600,
                ScheduledBy = "TestUser",
                ScheduledOn = now,
            };

            PriceChange pcPlus91 = new PriceChange()
            {
                Id = id,
                ChangesOn = now,
                Grade = plus91,
                GradeId = plus91.Id,
                IsPending = true,
                Price = 600,
                ScheduledBy = "TestUser",
                ScheduledOn = now,
            };

            PriceChange pcDiesel = new PriceChange()
            {
                Id = id,
                ChangesOn = now,
                Grade = diesel,
                GradeId = diesel.Id,
                IsPending = true,
                Price = 600,
                ScheduledBy = "TestUser",
                ScheduledOn = now,
            };

            PriceChange pcLpg = new PriceChange()
            {
                Id = id,
                ChangesOn = now,
                Grade = lpg,
                GradeId = lpg.Id,
                IsPending = true,
                Price = 600,
                ScheduledBy = "TestUser",
                ScheduledOn = now,
            };

            return new List<PriceChange>() { pcSuper, pcPlus91, pcDiesel, pcLpg };

        }

        public PriceChange GetPriceChange(int priceChangeId)
        {
            throw new NotImplementedException();
        }

        public PriceChangeState GetPriceChangeState(int PriceChangeStateId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PriceChange> GetAllPendingPriceChange(int latestPriceChangeId, bool includeGradeInfo = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PriceChangeState> GetJose(bool includeGradeInfo = false) {
            throw new NotImplementedException();
        }

    }
}
