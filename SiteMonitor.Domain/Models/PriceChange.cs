﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Domain.Models
{
    public class PriceChange
    {
        public int Id { get; set; }
        public Grade Grade { get; set; }
        public int GradeId { get; set; }
        [Required(ErrorMessage = "Debe ingresar un valor")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Debe ingresar un valor mayor a 0")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Debe ingresar una fecha")]
        public DateTime ChangesOn { get; set; }
        [Required] public DateTime ScheduledOn { get; set; }

        [Required] [MaxLength(150)] public string ScheduledBy { get; set; }

        [Required] public bool IsPending { get; set; }
    }
}
