﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteMonitor.Services;
using SiteMonitor.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;





namespace SiteMonitor.Web.Controllers
{

    public class LicenceController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILicenceService _licenceService;
        private readonly ISiteService _siteService;
        private readonly ILogger<LicenceController> _logger;

        public LicenceController(IMapper mapper,ILicenceService licenceService,ISiteService siteService, ILogger<LicenceController> logger)
        {
            _mapper = mapper;
            _licenceService = licenceService;
            _siteService = siteService;
            _logger = logger;
        }

        /*public LicenceController(ILicenceService licenceService)
        {
           
            _licenceService = licenceService;
         
        }*/

      
        [Authorize]
        // GET: Licence
        public IActionResult Index()
        {
            try
            {
                var states = _licenceService.GetLicenceStates(includeSiteInfo: true);
                var vm = _mapper.Map<List<LicenceStateViewModel>>(states);

                ViewBag.DataSource1 = vm;

                
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error");
                throw (ex);
            }


            return View();
        }
    }
}