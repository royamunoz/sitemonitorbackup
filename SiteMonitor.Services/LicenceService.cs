﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteMonitor.Data;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public class LicenceService : ILicenceService
    {
        private readonly ILicenceRepository _licenceRepository;

        public LicenceService(ILicenceRepository licenceRepository)
        {
            _licenceRepository = licenceRepository;
        }

        public IEnumerable<LicenceState> GetLicenceStates(bool includeSiteInfo=false)
        {            
            return _licenceRepository.GetLicenceStates(includeSiteInfo);
        }

        public async Task UpdateLicence(LicenceState state)
        {
            state.UpdatedOn = DateTime.UtcNow.AddHours(-6);
            await _licenceRepository.UpdateLicence(state);
        }

       
    }
}
