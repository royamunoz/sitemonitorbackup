﻿namespace SiteMonitor.Domain.Models
{
    public class PendingPriceChangeCheck
    {
        public int Id { get; set; }
        public int LatestPendingChange { get; set; }
    }
}
