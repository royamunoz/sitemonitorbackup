﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteMonitor.Domain.Models;
using SiteMonitor.Services;
using SiteMonitor.Web.ViewModels;
using Syncfusion.JavaScript;

namespace SiteMonitor.Web.Controllers
{

    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IPriceChangeService _priceChangeService;
        private readonly ISiteService _siteService;
        //private readonly ISiteRepository _siteRepository;
        private readonly IMapper _mapper;

        private readonly ILogger<AdminController> _logger;

        public AdminController(IPriceChangeService priceChangeService, ISiteService _siteService, IMapper mapper, ILogger<AdminController> logger)
        {
            this._priceChangeService = priceChangeService;
            this._siteService = _siteService;
            this._logger = logger;
            _mapper = mapper;
        }
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPriceChange()
        {
            try
            {
                var grades = _priceChangeService.GetGrades();

                AddPriceChangeViewModel viewModel = new AddPriceChangeViewModel()
                {
                    Grades = _mapper.Map<List<GradePriceViewModel>>(grades)
                };

                return View(viewModel);
            }
            catch(SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error.");
                throw (ex);
            }
            
        }

        [HttpPost]
        public ActionResult AddPriceChange(AddPriceChangeViewModel model)
        {
            if (ModelState.IsValid) {
                List<PriceChange> prices = new List<PriceChange>();
                for (int i = 0; i < model.Grades.Count; ++i)
                {
                    PriceChange priceChange = new PriceChange();
                    priceChange.ChangesOn = model.ChangesOn;
                    priceChange.ScheduledBy = User.Identity.Name;
                    priceChange.ScheduledOn = DateTime.Now;
                    priceChange.IsPending = true;
                    priceChange.GradeId = model.Grades[i].Id;
                    priceChange.Price = model.Grades[i].Price;
                    prices.Add(priceChange);
                    
                }

                try
                {
                    _priceChangeService.AddPriceChanges(prices);

                    //ViewBag.Success = "True";
                    //TempData["message"] = "True";
                    _logger.LogInformation("PriceChange created", prices);
                    TempData["mensaje"] = "exito";

                }
                catch (SqlException ex)
                {
                    _logger.LogError("Error connecting to database.{0}", ex);
                    TempData["mensaje"] = "error";
                    throw (ex);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error.");
                    TempData["mensaje"] = "error";
                    throw (ex);
                }

                return RedirectToAction(nameof(AddPriceChange));
            }
            return View(model);
            
        }


        public IActionResult UrlDatasource([FromBody]Data dm)
        {
            //List<Site> a = _siteService.GetActiveSites(onlyActiveSites: false);

            //IEnumerable<int> enumerable = Enumerable.Range(1, 300);
            IEnumerable<Site> order = _siteService.GetActiveSites(onlyActiveSites: false);
            List<Site> Data = order.ToList();

            
            //var Data = order.ToList();
            int count = order.Count();
            return dm.requiresCounts ? Json(new { result = Data.Skip(dm.skip).Take(dm.take), count = count }) : Json(Data);

            //var order = _siteService.GetActiveSites(onlyActiveSites: false).ToList();
            //var Data = order.ToList();
            //int count = order.Count();

            //return Json(new { result = Data, count = count });
        }

       
        //[HttpGet]
        public ActionResult SitesAdministration()
        {
            
            List<Site> sites = null;
            
            try
            {
                sites = _siteService.GetActiveSites(onlyActiveSites: false).ToList();
            }
            catch(SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error. {0}");
                throw (ex);
            }

            ViewBag.DataSource = sites;

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> EditSite(int siteId)
        {

            Site site;

            try
            {
                site = await _siteService.GetSiteAsync(siteId);
            }
            catch(SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error.");
                throw (ex);
            }


            if (site == null)
            {
                return NotFound();
            }
            return View(site);
        }

        [HttpPost]
        public async Task<IActionResult> EditSite(int id, [Bind("Id,Number,Name,IsActive,ReceivesPriceChanges")] Site site)
        {
            if (id != site.Id)
            {
                return NotFound();
            }

            try
            {
                if (ModelState.IsValid)
                {
                    if (site.Name.Length > 0)
                    {
                        await _siteService.UpdateSite(site);
                        _logger.LogInformation("PriceChange created", site);
                    }
                    return RedirectToAction(nameof(SitesAdministration));

                }
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error.");
                throw (ex);
            }

            return View(site);

        }

        public ActionResult Update([FromBody]CRUDModel<Site> value)
        {
            _siteService.UpdateSite(value.Value);
            return Json(value.Value);
        }

        //public ActionResult CrudUpdate([FromBody]Site value, string action)
        //{
        //    if (action == "update")
        //    {
        //        //Update record in database
        //        _siteService.UpdateSite(value);
        //    }
        

        public class Data
        {
            public bool requiresCounts { get; set; }
            public int skip { get; set; }
            public int take { get; set; }
        }
    }
}