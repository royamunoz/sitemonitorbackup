﻿using System;
using System.Collections.Generic;
using System.Text;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data.Helpers
{
    public static class PriceChangeGenerator
    {
        public static List<PriceChangeState> GetRandomPriceStateForSite(int site)
        {            
            Random r = new Random();
            if(r.Next()%2 == 0) 
                return new List<PriceChangeState>();

            DateTime now = DateTime.Now;
            PriceChangeState psSuper = GetPriceChangeState(site, 1, 600, now);
            PriceChangeState psPlus91 = GetPriceChangeState(site, 2, 550, now);
            PriceChangeState psDiesel = GetPriceChangeState(site, 3, 500, now);
            PriceChangeState psLpg = GetPriceChangeState(site, 4, 400, now);

            return new List<PriceChangeState>() { psSuper, psPlus91, psDiesel, psLpg };
        }

        private static PriceChangeState GetPriceChangeState(int site, int grade, decimal price, DateTime now)
        {
            return new PriceChangeState
            {
                SiteId = site,
                GradeId = grade,
                Price = price,
                ChangesOn = now,
                ScheduledOn = now,
                UpdatedOn = now
            };
        }
    }
}
