﻿using System.Collections.Generic;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public interface IPriceChangeService
    {
        List<SitePriceChangeSchedule> GetScheduledPricesForAllSites(bool includeGradeInfo);
        void UpdatePriceChange(PriceChangeUpdate lu);
        void AddPriceChange(PriceChange priceChange);
        void AddPriceChanges(IEnumerable<PriceChange> prices);
        //ScheduledPriceChangeResource GetScheduledPrices(PendingPriceChangeCheckResource data);
        List<Grade> GetGrades();
        List<PriceChange> GetPendingPriceChanges(int siteId, int latestPriceChangeId);
        PriceChange GetPriceChange(int priceChangeId);
        PriceChangeState GetPriceChangeState(int PriceChangeStateId);

        List<PriceChangeState> GetJose(bool includeGradeInfo);
    }
}