﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteMonitor.Services;
using System;
using System.Data.SqlClient;

namespace SiteMonitor.Web.Controllers
{

    public class PriceChangeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPriceChangeService _priceChangeService;
        private readonly ILogger<PriceChangeController> _logger;

        public PriceChangeController(IMapper mapper,IPriceChangeService priceChangeService, ILogger<PriceChangeController> logger)
        {
            _mapper = mapper;
            _priceChangeService = priceChangeService;
            _logger = logger;
        }

        [Authorize]
        // GET: PriceChange
        public ActionResult Index()
        {
            try
            {
                var data2 = _priceChangeService.GetJose(includeGradeInfo: true);
                var data = _priceChangeService.GetScheduledPricesForAllSites(includeGradeInfo: true);
                ViewBag.DataSource = data2;
            }
            catch (SqlException ex)
            {
                _logger.LogError("Error connecting to database. {0}", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error. {0}", ex);
                throw (ex);
            }

            return View();
        }

        public ActionResult SchedulePriceChange() {

            return View();
        }
    }
}