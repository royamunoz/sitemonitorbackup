﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class LicenceRepository:ILicenceRepository
    {

        private readonly AppDbContext _appDbContext;


        public LicenceRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<LicenceState> GetLicenceStates(bool includeSiteInfo = false)
        {
            if (includeSiteInfo)
                return _appDbContext.LicenceStates.Include(s => s.Site);
            else
                return _appDbContext.LicenceStates;
        }

        public async Task UpdateLicence(LicenceState state)
        {
            var oldState = _appDbContext.LicenceStates.FirstOrDefault(s => s.SiteId == state.SiteId);
            if (oldState != null)
                _appDbContext.LicenceStates.Remove(oldState);

            _appDbContext.LicenceStates.Add(state);

            await _appDbContext.SaveChangesAsync();
        }
    }
}
