﻿using Audit.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    [AuditDbContext(Mode = AuditOptionMode.OptOut, IncludeEntityObjects = false, AuditEventType = "{database}_{context}")]
    public class AppDbContext : AuditDbContext
    {
        public static readonly LoggerFactory ConsoleLoggerFactory = new LoggerFactory(new[]
        {
            new ConsoleLoggerProvider(
                (category, level) =>
                    category == DbLoggerCategory.Database.Command.Name && level == LogLevel.Information, true)
        });

        public AppDbContext(DbContextOptions<AppDbContext>options):base((DbContextOptions) options)
        {
            
        }

        public DbSet<SiteMonitor.Domain.Models.Grade> Grades { get; set; } // this tells the db context we want to store grades
        public DbSet<SiteMonitor.Domain.Models.Site> Sites { get; set; }
        public DbSet<SiteMonitor.Domain.Models.LicenceState> LicenceStates { get; set; }
        public DbSet<SiteMonitor.Domain.Models.PriceChangeState> PriceChangeStates { get; set; }
        public DbSet<SiteMonitor.Domain.Models.PriceChange> PriceChanges { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(ConsoleLoggerFactory)
                ;

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Site>()
            //    .HasAlternateKey(s => s.Number)
            //    .HasName("AK_Number");

            modelBuilder.Entity<Site>()
                .HasIndex(s => s.Number)
                .IsUnique();

            modelBuilder.Entity<Grade>()
                .HasKey(g => g.Id);
            modelBuilder.Entity<Grade>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
                

            modelBuilder.Entity<Grade>()
                .HasIndex(s => s.Name)
                .IsUnique();
        }

    }
}
