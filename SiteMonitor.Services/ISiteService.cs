﻿using System.Threading.Tasks;
using SiteMonitor.Domain.Models;
using System.Collections.Generic;


namespace SiteMonitor.Services
{
    public interface ISiteService
    {
        IEnumerable<Site> GetActiveSites(bool onlyActiveSites);
        Task<Site> GetSiteAsync(int siteId);
        Site GetSiteById(int id);
        Task UpdateSite(Site site);
    }
}