﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.ViewModels
{
    public class AddPriceChangeViewModel
    {
        
        [DataType(DataType.Date, ErrorMessage ="Ingrese una fecha")]
        //[DisplayFormat(DataFormatString = "{dd-MM-yyyy:0}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de Cambio")]
        [Required(ErrorMessage = "Date is required")]
        public DateTime ChangesOn { get; set; }
        public List<GradePriceViewModel> Grades { get; set; }

        public AddPriceChangeViewModel()
        {
            ChangesOn = DateTime.Today.AddDays(1).AddSeconds(-1);
            Grades = new List<GradePriceViewModel>();
        }

    }

    public class GradePriceViewModel
    {
        [ReadOnly(true)]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Combustible")]
        public string Name { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        [Display(Name = "Precio")]
        public decimal Price { get; set; }
    }
}
