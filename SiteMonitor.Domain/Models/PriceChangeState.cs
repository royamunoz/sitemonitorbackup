﻿using System;

namespace SiteMonitor.Domain.Models
{
    public class PriceChangeState
    {
        public int Id { get; set; }

        public Site Site { get; set; }
        public int SiteId { get; set; }

        public Grade Grade { get; set; }
        public int GradeId { get; set; }

        public decimal Price { get; set; }

        public DateTime ChangesOn { get; set; }
        public DateTime ScheduledOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
