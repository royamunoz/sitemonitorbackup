﻿using System.Collections.Generic;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public interface IPriceChangeRepository
    {
        IEnumerable<PriceChangeState> GetScheduledPricesForAllSites(bool includeGradeInfo);
        IEnumerable<PriceChangeState> GetScheduledPricesForSite(int siteId, bool includeGradeInfo);
        IEnumerable<PriceChange> GetAllPendingPriceChange(bool includeGradeInfo);
        IEnumerable<PriceChange> GetAllPendingPriceChange(int latestPriceChangeId, bool includeGradeInfo);
        void AddPriceChange(PriceChange priceChange);
        void AddPriceChanges(IEnumerable<PriceChange> prices);
        IEnumerable<Grade> GetGrades();
        IEnumerable<PriceChangeState> GetJose(bool includeGradeInfo);
        PriceChange GetPriceChange(int priceChangeId);
        PriceChangeState GetPriceChangeState(int PriceChangeStateId);
    }
}