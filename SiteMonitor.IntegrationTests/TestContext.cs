﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using SiteMonitor.Data;
using SiteMonitor.Web;
using System.Net.Http;

namespace SiteMonitor.IntegrationTests
{
    class TestContext
    {
        private readonly HttpClient _client;
        private readonly TestServer _testServer;
        public readonly AppDbContext context;

        public TestContext()
        {
            var configuration = new ConfigurationBuilder().Build();

            var builder = new WebHostBuilder()
                      // Set test environment
                      .UseEnvironment("Testing")
                      .UseStartup<Startup>()
                      .UseConfiguration(configuration);

            _testServer = new TestServer(builder);
            context = _testServer.Host.Services.GetService(typeof(AppDbContext)) as AppDbContext;
            _client = _testServer.CreateClient();
        }
    }
}
