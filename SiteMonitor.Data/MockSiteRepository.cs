﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class MockSiteRepository : ISiteRepository
    {
        public IEnumerable<Site> GetActiveSites(bool onlyActiveSites = true)
        {
            List<Site> activeSites = new List<Site>();
            for (int i = 0; i < 50; i++)
                activeSites.Add(GetFakeSite(i));

            return activeSites;

        }

        public Task<Site> GetSiteAsync(int siteId)
        {
            return new Task<Site>(() => GetFakeSite(siteId)); 
        }

        private Site GetFakeSite(int siteId)
        {
            return new Site() {Id = siteId,Name = $"Site {siteId}", IsActive = true};
        }


        public Site GetSiteById(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task UpdateSite(Site site)
        {
            throw new System.NotImplementedException();
        }
    }
}
