﻿using System;

namespace SiteMonitor.Domain.Models
{
    public class LicenceState
    {
        public int Id { get; set; }

        public Site Site { get; set; }
        public int SiteId { get; set; }

        public DateTime ExpirationDate { get; set; }
        public DateTime UpdatedOn { get; set; }

        
    }
}
