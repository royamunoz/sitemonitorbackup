﻿using System;

namespace SiteMonitor.Data.Helpers
{
    public static class DateGenerator
    {
        public static DateTime FutureDate(int maxDays)
        {
            var random = new Random();
            return DateTime.Now.AddDays(random.Next(maxDays));            
        }

        public static DateTime ExpirationDate(int maxDays)
        {
            return FutureDate(maxDays).Date.AddHours(14);
        }

        public static DateTime PastDate(int maxDays)
        {
            var random = new Random();
            return DateTime.Now.AddDays(-random.Next(maxDays));
        }

        public static DateTime UpdatedOnDate(int hoursInThePast)
        {
            var random = new Random();
            return PastDate(0).Date.AddHours(-random.Next(hoursInThePast));
        }
    }
}