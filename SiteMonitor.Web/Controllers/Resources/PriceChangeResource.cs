﻿using System;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class PriceChangeResource
    {
        public int Grade { get; set; }
        public decimal Price { get; set; }
        public DateTime ChangesOn { get; set; }
    }
}