﻿using System;
using System.Linq;
using System.Collections.Generic;
using SiteMonitor.Data;
using SiteMonitor.Domain.Models;
using SiteMonitor.Services.Services;


namespace SiteMonitor.Services
{
    public class DashboardService : IDashboardService
    {
        private const int WarningDays = 5;
        private const int CriticalDays = 3;

        private readonly ILicenceRepository _licenceRepository;
        private readonly IPriceChangeRepository _priceChangeRepository;
        private readonly ISiteRepository _siteRepository;

        public DashboardService(ILicenceRepository licenceRepository, IPriceChangeRepository priceChangeRepository,
            ISiteRepository siteRepository)
        {
            _licenceRepository = licenceRepository;
            _priceChangeRepository = priceChangeRepository;
            _siteRepository = siteRepository;
        }

        public DashboardData GetDashboardData()
        {
            var siteCount = Enumerable.Count<Site>(_siteRepository.GetActiveSites(onlyActiveSites:true));
            var priceChangeCount = Enumerable.Select<PriceChangeState, int>(_priceChangeRepository.GetScheduledPricesForAllSites(includeGradeInfo:false), x => x.SiteId)
                   .Distinct().Count();

            var nonPriceChangeCount = siteCount - priceChangeCount;

            //var licenceData = _licenceRepository.GetLicenceStates().ToList();
            var licenceData = Enumerable.Select<LicenceState, double>(_licenceRepository.GetLicenceStates(includeSiteInfo:false), x=> (x.ExpirationDate - DateTime.Now).TotalDays ).ToList();
            var okLicenceCount = licenceData
                //.Count(d => (DateTime.Now - d.ExpirationDate).TotalDays > WarningDays);
                .Count(d => d > WarningDays);

            var warningLicenceCount = licenceData
                //.Count(d => ((DateTime.Now - d.ExpirationDate).TotalDays > CriticalDays &&
                //             (DateTime.Now - d.ExpirationDate).TotalDays <= WarningDays));
                .Count(d => d > CriticalDays && d <= WarningDays);

            var criticalLicenceCount = licenceData
            //.Count(d => (DateTime.Now - d.ExpirationDate).TotalDays <= CriticalDays);
                .Count(d => d <= CriticalDays);

            Dictionary<string, string> dict1 = new Dictionary<string, string>();
            dict1.Add("criticalLicenceCount", (Convert.ToDouble((Convert.ToDouble(criticalLicenceCount) / Convert.ToDouble(siteCount)) * 100)).ToString("0.0"));
            dict1.Add("warningLicenceCount", (Convert.ToDouble((Convert.ToDouble(warningLicenceCount) / Convert.ToDouble(siteCount)) * 100)).ToString("0.0"));
            dict1.Add("okLicenceCount", (Convert.ToDouble((Convert.ToDouble(okLicenceCount) / Convert.ToDouble(siteCount)) * 100)).ToString("0.0"));


            Dictionary<string, string> dict2 = new Dictionary<string, string>();
            dict2.Add("priceChangeCount", (Convert.ToDouble((Convert.ToDouble(priceChangeCount) / Convert.ToDouble(siteCount)) * 100)).ToString("0.0"));
            dict2.Add("nonPriceChangeCount", (100- Convert.ToDouble(((Convert.ToDouble(priceChangeCount) / Convert.ToDouble(siteCount)) * 100))).ToString("0.0"));



            var priceChanges = (_priceChangeRepository.GetAllPendingPriceChange(includeGradeInfo:true)).ToList();
            return new DashboardData
            {
                SiteCount = siteCount,
                SitesWithScheduledPricesCount = priceChangeCount,
                SitesWithoutScheduledPricesCount = nonPriceChangeCount,
                OkLicenceCount = okLicenceCount,
                WarningLicenceCount = warningLicenceCount,
                CriticalLicenceCount = criticalLicenceCount,
                PriceChanges = priceChanges,
                dictionaryChart1=dict1,
                dictionaryChart2=dict2
            };

        }
    }
}
