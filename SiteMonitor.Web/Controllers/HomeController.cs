﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SiteMonitor.Services.Services;
using SiteMonitor.Web.ViewModels;
using Syncfusion.JavaScript.Models;
using Syncfusion.EJ.Export;
using Syncfusion.XlsIO;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;

namespace SiteMonitor.Web.Controllers
{
    //TODO: Implemente EF Core Repository for reading data from an actual DB
    //TODO: Implement API for receiveing updates from sites
    //TODO: Implement API for sending price changes to sites
    //TODO: Implement API for sending licence updates to sites
    

    [Authorize]
    public class HomeController : Controller
    {
        private readonly IDashboardService _dashboardService;

        private readonly ILogger<HomeController> _logger;

        public HomeController(IDashboardService dashboardService, ILogger<HomeController> logger)
        {
            _dashboardService = dashboardService;
            _logger = logger;
        }

        public IActionResult Index()
        {

            //_logger.LogCritical("Error connecting to database.");

            //var client = new RestClient("https://josevalverde.auth0.com/api/v2/users");
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("content-type", "application/json");
            //request.AddHeader("authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1qVXhOelV3TmpJelJEQkVPRFkyUmtNMlJqVTJRekEyTkRjNFFUTTROa1kzTmtNeE56bEdNdyJ9.eyJpc3MiOiJodHRwczovL2pvc2V2YWx2ZXJkZS5hdXRoMC5jb20vIiwic3ViIjoiR2loekNIc1hGdWJ0OElYVkRtNUNvdWJXYmdUUnhENzdAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vam9zZXZhbHZlcmRlLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTM3NTQ5NjA2LCJleHAiOjE1Mzc2MzYwMDYsImF6cCI6IkdpaHpDSHNYRnVidDhJWFZEbTVDb3ViV2JnVFJ4RDc3Iiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcl90aWNrZXRzIHJlYWQ6Y2xpZW50cyB1cGRhdGU6Y2xpZW50cyBkZWxldGU6Y2xpZW50cyBjcmVhdGU6Y2xpZW50cyByZWFkOmNsaWVudF9rZXlzIHVwZGF0ZTpjbGllbnRfa2V5cyBkZWxldGU6Y2xpZW50X2tleXMgY3JlYXRlOmNsaWVudF9rZXlzIHJlYWQ6Y29ubmVjdGlvbnMgdXBkYXRlOmNvbm5lY3Rpb25zIGRlbGV0ZTpjb25uZWN0aW9ucyBjcmVhdGU6Y29ubmVjdGlvbnMgcmVhZDpyZXNvdXJjZV9zZXJ2ZXJzIHVwZGF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGRlbGV0ZTpyZXNvdXJjZV9zZXJ2ZXJzIGNyZWF0ZTpyZXNvdXJjZV9zZXJ2ZXJzIHJlYWQ6ZGV2aWNlX2NyZWRlbnRpYWxzIHVwZGF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgZGVsZXRlOmRldmljZV9jcmVkZW50aWFscyBjcmVhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIHJlYWQ6cnVsZXMgdXBkYXRlOnJ1bGVzIGRlbGV0ZTpydWxlcyBjcmVhdGU6cnVsZXMgcmVhZDpydWxlc19jb25maWdzIHVwZGF0ZTpydWxlc19jb25maWdzIGRlbGV0ZTpydWxlc19jb25maWdzIHJlYWQ6ZW1haWxfcHJvdmlkZXIgdXBkYXRlOmVtYWlsX3Byb3ZpZGVyIGRlbGV0ZTplbWFpbF9wcm92aWRlciBjcmVhdGU6ZW1haWxfcHJvdmlkZXIgYmxhY2tsaXN0OnRva2VucyByZWFkOnN0YXRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6c2hpZWxkcyBjcmVhdGU6c2hpZWxkcyBkZWxldGU6c2hpZWxkcyB1cGRhdGU6dHJpZ2dlcnMgcmVhZDp0cmlnZ2VycyByZWFkOmdyYW50cyBkZWxldGU6Z3JhbnRzIHJlYWQ6Z3VhcmRpYW5fZmFjdG9ycyB1cGRhdGU6Z3VhcmRpYW5fZmFjdG9ycyByZWFkOmd1YXJkaWFuX2Vucm9sbG1lbnRzIGRlbGV0ZTpndWFyZGlhbl9lbnJvbGxtZW50cyBjcmVhdGU6Z3VhcmRpYW5fZW5yb2xsbWVudF90aWNrZXRzIHJlYWQ6dXNlcl9pZHBfdG9rZW5zIGNyZWF0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIGRlbGV0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIHJlYWQ6Y3VzdG9tX2RvbWFpbnMgZGVsZXRlOmN1c3RvbV9kb21haW5zIGNyZWF0ZTpjdXN0b21fZG9tYWlucyByZWFkOmVtYWlsX3RlbXBsYXRlcyBjcmVhdGU6ZW1haWxfdGVtcGxhdGVzIHVwZGF0ZTplbWFpbF90ZW1wbGF0ZXMiLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.zEQnwbdDwdAS321zvVFAgOsHrnPlu_C0uCvZ-zndSelywRZr3cN7wv1uBtwdaMVP0xn8cGvYHl5AiLIlNueHKKv40rFSho8QcPHrJ9-PbydV-7uYEAKdbfUZig4B33hKwoJ2J5LsUGVORBLaGZHDUfOAQMNBS4G4z4uBMDHvxd2hIXjsVtprT665mnOOHDuQC7TtIQi6BG8InpqNNt15rCkCL-VakvKA5FnTKIiDooyD2wot1pNpaqLegYi2XFFEbZ52KZKesUDUNNjZZgYdHMcwhNBw_GDb0V544eJgINLBvVeRS03mK924sq23ovCWZtbgUM0h2bBaOb453SQhrg");
            //request.AddParameter("application/json", "{ \"connection\": \"Username-Password-Authentication\" , \"email\": \"jane.doetest@example.com\", \"password\": \"Prueba666jjj\", \"user_metadata\": {\"hobby\": \"surfing\"}, \"app_metadata\": {\"plan\": \"full\"}}", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);

            //var config = ConfigurationManager.
            //    .OpenExeConfiguration(ConfigurationUserLevel.None);
            //var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            //connectionStringsSection.ConnectionStrings["Blah"].ConnectionString = "Data Source=blah;Initial Catalog=blah;UID=blah;password=blah";
            //config.Save();
            //ConfigurationManager.RefreshSection("connectionStrings");

            try
            {
                var dashboardData = _dashboardService.GetDashboardData();
                //float valor = (Convert.ToInt32(dashboardData.CriticalLicenceCount) / 48)*100;
                var DataSource = _dashboardService.GetDashboardData().PriceChanges;
                ViewBag.DataSource = DataSource;

                List<DoughnutData> chartScheduledSitesData = new List<DoughnutData>
            {
                new DoughnutData { xValue =  "Con sitios programados", yValue = Convert.ToDouble(_dashboardService.GetDashboardData().dictionaryChart2["priceChangeCount"]) , text = _dashboardService.GetDashboardData().SitesWithScheduledPricesCount.ToString() },
                new DoughnutData { xValue =  "Sin sitios programados", yValue = Convert.ToDouble(_dashboardService.GetDashboardData().dictionaryChart2["nonPriceChangeCount"]), text = _dashboardService.GetDashboardData().SitesWithoutScheduledPricesCount.ToString() },
            };

                List<DoughnutData> chartSitesData = new List<DoughnutData>
            {
                new DoughnutData { xValue =  "Críticas", yValue =Convert.ToDouble(_dashboardService.GetDashboardData().dictionaryChart1["criticalLicenceCount"]), text = _dashboardService.GetDashboardData().CriticalLicenceCount.ToString() },
                new DoughnutData { xValue =  "En alerta", yValue = Convert.ToDouble(_dashboardService.GetDashboardData().dictionaryChart1["warningLicenceCount"]), text =  _dashboardService.GetDashboardData().WarningLicenceCount.ToString() },
                new DoughnutData { xValue =  "OK", yValue = Convert.ToDouble(_dashboardService.GetDashboardData().dictionaryChart1["okLicenceCount"]), text =  _dashboardService.GetDashboardData().OkLicenceCount.ToString()},

                
            };

                ViewBag.dataSourceScheduled = chartScheduledSitesData;
                ViewBag.dataSourceSites = chartSitesData;
                ViewBag.SitesCount = _dashboardService.GetDashboardData().SiteCount;

                return View(dashboardData);
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error.");
                throw (ex);
            }
        }      

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize(Roles = "admin")]
        public IActionResult Admin()
        {
            return View();
        }


        public ActionResult pie()
        {
            try
            {
                var DataSource = _dashboardService.GetDashboardData().PriceChanges;
                ViewBag.DataSource = DataSource;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error connecting to database.");
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error.");
                throw (ex);
            }

            return View();

         
        }
      
        public ActionResult ExportToExcel(string GridModel)
        {
            try
            {
                ExcelExport exp = new ExcelExport();
                var DataSource = _dashboardService.GetDashboardData().PriceChanges;
                GridProperties gridProp = ConvertGridObject(GridModel);
                GridExcelExport excelExp = new GridExcelExport();
                excelExp.FileName = "Export.xlsx"; excelExp.Excelversion = ExcelVersion.Excel2010;
                excelExp.Theme = "flat-saffron";
                var actionExport = exp.Export(gridProp, DataSource, excelExp);
                _logger.LogInformation("Excel file downloaded");
                return actionExport;
            }
            catch (SqlException ex)
            {
                _logger.LogError("Error connecting to database.{0}", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error. {0}", ex);
                throw (ex);
            }
            
        }

        public ActionResult ExportToWord(string GridModel)
        {
            try
            {
                WordExport exp = new WordExport();
                var DataSource = _dashboardService.GetDashboardData().PriceChanges;
                GridProperties gridProp = ConvertGridObject(GridModel);
                GridWordExport wrdExp = new GridWordExport();
                wrdExp.FileName = "Export.docx"; wrdExp.Theme = "flat-saffron";
                var actionExport = exp.Export(gridProp, DataSource, wrdExp);
                _logger.LogInformation("Word file downloaded");
                return actionExport;

            }

            catch (SqlException ex)
            {
                _logger.LogError("Error connecting to database.{0}", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error. {0}", ex);
                throw (ex);
            }
        }


        //[Route("Home")]
        //[Route("Home/Index")]
        
        public ActionResult ExportToPdf(string GridModel)
        {
            try
            {
                PdfExport exp = new PdfExport();
                var DataSource = _dashboardService.GetDashboardData().PriceChanges;
                GridProperties gridProp = ConvertGridObject(GridModel);
                GridPdfExport pdfExp = new GridPdfExport();
                pdfExp.FileName = "Export.pdf"; pdfExp.Theme = "flat-saffron";
                var actionExport = exp.Export(gridProp, DataSource, pdfExp);
                _logger.LogInformation("PDF file downloaded");
                return actionExport;

            }
            catch (SqlException ex)
            {
                _logger.LogError("Error connecting to database.{0}", ex);
                throw (ex);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error. ", ex);
                throw (ex);
            }
        }
        private GridProperties ConvertGridObject(string gridProperty)
        {
            GridProperties gridProp = new GridProperties();
            gridProp = (GridProperties)JsonConvert.DeserializeObject(gridProperty, typeof(GridProperties));
            return gridProp;
        }

        public class DoughnutData
        {
            public string xValue;
            public double yValue;
            public string text;
        }



    }
}
