﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SiteMonitor.Web.Controllers
{
    
    public class AccountController : Controller
    {
        private readonly ILogger _logger;

        public AccountController(ILogger<AccountController> logger)
        {
            _logger = logger;
        }

  
        [HttpGet]
        //[AllowAnonymous]
        public async Task Login(string returnUrl = "/")
        {
            try
            {
                await HttpContext.ChallengeAsync("Auth0", new AuthenticationProperties() { RedirectUri = returnUrl });
                _logger.LogInformation("User logged in", User.Identity);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error logging in");
                throw (ex);
            }
        }

        [Authorize]
        public async Task Logout()
        {
            try
            {
                await HttpContext.SignOutAsync("Auth0", new AuthenticationProperties
                {
                    // Indicate here where Auth0 should redirect the user after a logout.
                    // Note that the resulting absolute Uri must be whitelisted in the 
                    // **Allowed Logout URLs** settings for the client.
                    RedirectUri = Url.Action("Index", "Home")
                });
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                _logger.LogInformation("User logged out", User.Identity);

            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error logging out");
                throw (ex);
            }
        }

  
        [Authorize]
        public async Task<IActionResult> Claims()
        {
            if (User.Identity.IsAuthenticated)
            {
                string accessToken = await HttpContext.GetTokenAsync("access_token");
                string idToken = await HttpContext.GetTokenAsync("id_token");
                string role = "";

                //string claimvalue = ((System.Security.Claims.ClaimsIdentity)User.Identity).FindFirst("https://josevalverde.example/roles").Value;


                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                 IEnumerable<Claim> claims = identity.Claims;
                    role = identity.FindFirst("https://schemas.gaspro.cr/roles").Value;
                    string claimvalue = ((ClaimsIdentity)User.Identity).FindFirst("https://schemas.gaspro.cr/roles").Value;

                }

            }
            return View();
        }
    }
}