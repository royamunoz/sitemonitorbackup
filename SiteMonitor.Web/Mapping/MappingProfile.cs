﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SiteMonitor.Domain.Models;
using SiteMonitor.Services;
using SiteMonitor.Web.Controllers.Resources;
using SiteMonitor.Web.ViewModels;

namespace SiteMonitor.Web.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UpdateLicenceResource, LicenceState>();
            CreateMap<PriceChangeResource, PriceChange>();
            CreateMap<PriceChangeUpdateResource, PriceChangeUpdate>();
            CreateMap<PendingPriceChangeCheckResource, PendingPriceChangeCheckResource>();

            CreateMap<LicenceState, LicenceStateViewModel>()
                .ForMember(vm => vm.Id, opt => opt.MapFrom(lu => lu.Site.Id))
                .ForMember(vm => vm.Name, opt => opt.MapFrom(lu => lu.Site.Name))
                .ForMember(vm => vm.ExpirationDate, opt => opt.MapFrom(lu => lu.ExpirationDate))
                .ForMember(vm => vm.UpdatedOn, opt => opt.MapFrom(lu => lu.UpdatedOn))
                .ForMember(vm => vm.RemainingDays,
                    opt => opt.ResolveUsing(lu => (int) Math.Ceiling((lu.ExpirationDate - DateTime.Now).TotalDays)))
                ;

            CreateMap<PriceChangeState, PriceChangeData>();

            CreateMap<Grade, GradePriceViewModel>()
                .ReverseMap();

            CreateMap<PriceChange,PendingPriceChangeResource>();
        }
    }
}
