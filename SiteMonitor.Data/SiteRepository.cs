﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class SiteRepository:ISiteRepository
    {

        private readonly AppDbContext _appDbContext;

        public SiteRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public IEnumerable<Site> GetActiveSites(bool onlyActiveSites = true)
        {
            if (onlyActiveSites)
                return _appDbContext.Sites.Where(s => s.IsActive == true);

            return _appDbContext.Sites;
        }

        public Task<Site> GetSiteAsync(int siteId)
        {
            return _appDbContext.Sites.FirstOrDefaultAsync(s => s.Id == siteId);
        }

        public Site GetSiteById(int id)
        {
            return _appDbContext.Sites.FirstOrDefault(s => s.Id == id);
        }

        public async Task UpdateSite(Site site)
        {
            _appDbContext.Update(site);
            await _appDbContext.SaveChangesAsync();
        }

        
    }
}
