﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SiteMonitor.Data;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public class SiteService : ISiteService
    {
        private readonly ISiteRepository _siteRepository;

        public SiteService(ISiteRepository siteRepository)
        {
            _siteRepository = siteRepository;
        }

        public Task<Site> GetSiteAsync(int siteId)
        {
            return _siteRepository.GetSiteAsync(siteId);
        }

        public IEnumerable<Site> GetActiveSites(bool onlyActiveSites = true)
        {
            return _siteRepository.GetActiveSites(onlyActiveSites);
        }

        public Site GetSiteById(int id)
        {
            return _siteRepository.GetSiteById(id);
        }

        public async Task UpdateSite(Site site) {
           await _siteRepository.UpdateSite(site);
        }

    }
}
