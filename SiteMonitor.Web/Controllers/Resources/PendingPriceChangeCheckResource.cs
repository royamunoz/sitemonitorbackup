﻿using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class PendingPriceChangeCheckResource
    {
        [Required]
        public int SiteNumber { get; set; }
        [Required]
        public int LatestChange { get; set; }
    }
}
