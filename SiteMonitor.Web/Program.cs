﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SiteMonitor.Data;
using System;
using NLog.Web;
using Microsoft.Extensions.Logging;

namespace SiteMonitor.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // NLog: setup the logger first to catch all errors
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            var host = BuildWebHost(args);

            try
            {
                logger.Debug("init main");
            }
            catch (Exception ex)
            {
                //NLog: catch setup errors
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    //var context = services.GetRequiredService<AppDbContext>();
                    //DbInitializer.Seed(context);
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex);
                }
            }

            host.Run();
            NLog.LogManager.Shutdown();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .ConfigureLogging(logging =>
        {
            logging.ClearProviders();
            logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Warning);
        })
        .UseNLog()  // NLog: setup NLog for Dependency injection
        .Build();
    }
}
