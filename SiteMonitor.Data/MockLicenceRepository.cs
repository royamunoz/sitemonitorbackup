﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteMonitor.Data.Helpers;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Data
{
    public class MockLicenceRepository : ILicenceRepository
    {
        public IEnumerable<LicenceState> GetLicenceStates(bool includeSiteInfo = false)
        {
            List<LicenceState> states = new List<LicenceState>();

            for (int i = 0; i < 50; i++)
                states.Add(GetRandomLicenceStateModel(i));

            return states;
        }

        public Task UpdateLicence(LicenceState state)
        {
            return Task.CompletedTask;
        }

        private LicenceState GetRandomLicenceStateModel(int siteId)
        {
            Site site = new Site() { Id = siteId + 1, Name = $"Test Site {siteId}", IsActive = true };
            DateTime expirationDate = DateGenerator.ExpirationDate(31);
            DateTime updatedOn = DateGenerator.UpdatedOnDate(36);
            LicenceState licenceState = new LicenceState() { Site = site, ExpirationDate = expirationDate, Id = 0, };

            return licenceState;
        }


    }
}
