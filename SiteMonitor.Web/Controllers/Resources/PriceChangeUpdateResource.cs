﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class PriceChangeUpdateResource
    {
        public int SiteNumber { get; set; }

        public List<PriceChangeResource> Schedule { get; set; }

        public PriceChangeUpdateResource()
        {
            Schedule = new List<PriceChangeResource>();
        }
    }
}
