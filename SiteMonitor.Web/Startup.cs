﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.AzureAppServices;
using Microsoft.IdentityModel.Tokens;
using SiteMonitor.Data;
using SiteMonitor.Services;
using SiteMonitor.Services.Services;
using System;
using System.Diagnostics;
using System.Threading.Tasks;




namespace SiteMonitor.Web
{
    public class Startup
    {
        private IHostingEnvironment env { get; set; }

        public Startup(IHostingEnvironment CurrentEnvironment, IConfiguration configuration)
        {
            Configuration = configuration;
            env = CurrentEnvironment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddMvc();

            //services.AddMvc().
            //AddJsonOptions(options =>
            //{
            //    // JSON serialization not defaulting to default?
            //    options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            //});
            Mapper.Reset();
            services.AddAutoMapper();

            if (env.IsEnvironment("Testing"))
            {
                /*Uses an in-memory database to run integration tests. A GUID is generates to use as a name for the database 
                 * to ensure that every run has new database that is not affected by other test runs.*/
                services.AddDbContext<AppDbContext>(options => options.UseInMemoryDatabase(Guid.NewGuid().ToString()));
            }
            else
            {
                // EF Core setup. Use sql server and read the connection string from the settings section ConnectionStrings - DefaultConnection
                services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            }

            services.AddTransient<ILicenceService, LicenceService>();
            services.AddTransient<IPriceChangeService, PriceChangeService>();
            services.AddTransient<IDashboardService, DashboardService>();
            services.AddTransient<ISiteService, SiteService>();


            //services.AddTransient<ILicenceRepository, MockLicenceRepository>();
            //services.AddTransient<IPriceChangeRepository, MockPriceChangeRepository>();
            //services.AddTransient<ISiteReposeitory, MockSiteRepository>();

            services.AddTransient<ILicenceRepository, LicenceRepository>();
            services.AddTransient<IPriceChangeRepository, PriceChangeRepository>();
            services.AddTransient<ISiteRepository, SiteRepository>();


            //services.Configure(Configuration.GetSection(""));

            // Auth0 setup

            // Add authentication services
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
                .AddCookie()
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = Configuration["Auth0:Domain"];
                    options.Audience = Configuration["Auth0:ApiIdentifier"];
                })
                .AddOpenIdConnect("Auth0", options =>
                {
                    // Set the authority to your Auth0 domain
                    options.Authority = $"{Configuration["Auth0:Domain"]}";

                    // Configure the Auth0 Client ID and Client Secret
                    options.ClientId = Configuration["Auth0:ClientId"];
                    options.ClientSecret = Configuration["Auth0:ClientSecret"];

                    // Set response type to code
                    options.ResponseType = "code";

                    // Configure the scope
                    options.Scope.Clear();
                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.Scope.Add("email");

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = Configuration["Auth0:RoleClaim"]
                    };

                    // Set the callback path, so Auth0 will call back to http://localhost:5000/signin-auth0 
                    // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard 
                    options.CallbackPath = new PathString("/signin-auth0");

                    // Configure the Claims Issuer to be Auth0
                    options.ClaimsIssuer = "Auth0";

                    options.SaveTokens = true;

                    options.Events = new OpenIdConnectEvents
                    {
                        OnRedirectToIdentityProvider = context =>
                        {
                            context.ProtocolMessage.SetParameter("audience", Configuration["Auth0:ApiUrl"]);
                            return Task.FromResult(0);
                        },
                        // handle the logout redirection 
                        OnRedirectToIdentityProviderForSignOut = (context) =>
                        {
                            var logoutUri =
                                $"{Configuration["Auth0:Domain"]}/v2/logout?client_id={Configuration["Auth0:ClientId"]}";

                            var postLogoutUri = context.Properties.RedirectUri;
                            if (!string.IsNullOrEmpty(postLogoutUri))
                            {
                                if (postLogoutUri.StartsWith("/"))
                                {
                                    // transform to absolute
                                    var request = context.Request;
                                    postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase +
                                                    postLogoutUri;
                                }
                                logoutUri += $"&returnTo={Uri.EscapeDataString(postLogoutUri)}";
                            }

                            context.Response.Redirect(logoutUri);
                            context.HandleResponse();

                            return Task.CompletedTask;
                        }
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            //Audit.Core.Configuration.Setup()
            //.UseFileLogProvider(config => config
            //    .FilenamePrefix("AuditLog_")
            //    .Directory(@"C:\temp\audit\Development"))
            //.WithCreationPolicy(EventCreationPolicy.InsertOnStartReplaceOnEnd)
            //.WithAction(x => x.OnScopeCreated(scope => scope.SetCustomField("ApplicationName", "SiteMonitor")));

            // syncfusion v16.3.0.21 unlock key
            // @31362e332e30PKAkC4E+KWlTbJjf2kBpi9rBHTulnXT/tbnirlGRWl0= Configuration["Auth0:ClientId"]
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(Configuration["Syncfusion:LicenseKey"]);

            //Configuration to store audit logs in Azure Blobs

            Audit.Core.Configuration.Setup()
            .UseAzureBlobStorage(config => config
            .ConnectionString(Configuration["Azure:ConnectionString"])
            .ContainerName(Configuration["Azure:AuditContainer"]) //have to be in lowercase
            .BlobNameBuilder(ev => $"{ev.StartDate:yyyy-MM}/{ev.Environment.UserName}/AuditLog_{Guid.NewGuid()}.json"));


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var timingLogger = loggerFactory.CreateLogger("SiteMonitor Log");

            app.Use(async (HttpContext context, Func<Task> next) =>
            {
                var timer = new Stopwatch();
                timer.Start();

                await next();

                timer.Stop();

                timingLogger.LogInformation($"Request to {context.Request.Method}:{context.Request.Path} processed in {timer.ElapsedMilliseconds} ms");

            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });
        }
    }
}
