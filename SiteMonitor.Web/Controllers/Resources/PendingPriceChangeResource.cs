﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class PendingPriceChangeResource
    {
        public int Id { get; set; }
        public int GradeId { get; set; }
        public decimal Price { get; set; }
        public DateTime ChangesOn { get; set; }
    }
}
