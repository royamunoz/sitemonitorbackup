﻿using SiteMonitor.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitor.Web.ViewModels
{
    public class SchedulePriceChangeViewModel
    {
        public List<Grade> Grades { get; set; }
        public List<PriceChange> Schedule { get; set; }
    }
}
