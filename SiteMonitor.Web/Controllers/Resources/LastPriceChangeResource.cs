﻿using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Web.Controllers.Resources
{
    public class LastPriceChangeResource
    {

        [Required]
        public int SiteNumber { get; set; }
        [Required]
        public int LatestPriceChangeId { get; set; }
    }
}
