﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SiteMonitor.Data;
using SiteMonitor.Domain.Models;

namespace SiteMonitor.Services
{
    public class PriceChangeService : IPriceChangeService
    {
        private readonly IPriceChangeRepository _priceChangeRepository;
        private readonly ISiteRepository _siteRepository;
        private readonly IMapper _mapper;

        public PriceChangeService(IPriceChangeRepository priceChangeRepository, ISiteRepository siteRepository, IMapper mapper)
        {
            _priceChangeRepository = priceChangeRepository;
            _siteRepository = siteRepository;
            _mapper = mapper;
        }        

        public List<SitePriceChangeSchedule> GetScheduledPricesForAllSites(bool includeGradeInfo=false)
        {
            List<SitePriceChangeSchedule> schedule = new List<SitePriceChangeSchedule>();
            var sites = _siteRepository.GetActiveSites(onlyActiveSites:true);

            foreach (var site in sites)
            {
                SitePriceChangeSchedule npvm = new SitePriceChangeSchedule() { Site = site};

                IEnumerable<PriceChangeState> siteSchedule = _priceChangeRepository.GetScheduledPricesForSite(site.Id,includeGradeInfo);
                foreach (var state in siteSchedule)
                    npvm.Schedule.Add(_mapper.Map<PriceChangeData>(state));
                
                schedule.Add(npvm);
            }

            return schedule;

            /* List < SitePriceChangeSchedule > schedule = new List<SitePriceChangeSchedule>();
            var sites = _siteRepository.GetActiveSites(onlyActiveSites: true);

            foreach (var site in sites)
            {
                SitePriceChangeSchedule npvm = new SitePriceChangeSchedule() { Site = site };

                IEnumerable<PriceChangeState> siteSchedule = _priceChangeRepository.GetScheduledPricesForSite(site.Id, includeGradeInfo);
                foreach (var state in siteSchedule)
                    npvm.Schedule.Add(_mapper.Map<PriceChangeData>(state));

                schedule.Add(npvm);
            }

            return schedule; 
             
             
             
             */
        }

       



        public void UpdatePriceChange(PriceChangeUpdate lu)
        {
            // check if site exists
            // delete current info
            // insert new info
        }

        public void AddPriceChange(PriceChange priceChange)
        {
            _priceChangeRepository.AddPriceChange(priceChange);
        }

        public void AddPriceChanges(IEnumerable<PriceChange> prices)
        {
            _priceChangeRepository.AddPriceChanges(prices);
        }

        public List<Grade> GetGrades()
        {
            return _priceChangeRepository.GetGrades().ToList();
        }

        public List<PriceChange> GetPendingPriceChanges(int siteId, int latestPriceChangeId)
        {
            List<PriceChange> prices = new List<PriceChange>();
            var site = _siteRepository.GetSiteById(siteId);

            //check if site from params, receive price changes
            if (!site.ReceivesPriceChanges)
                return prices;

            var allChanges = _priceChangeRepository.GetAllPendingPriceChange(latestPriceChangeId, includeGradeInfo: false);
            prices.AddRange(allChanges);
            return prices;

        }

        public List<PriceChange> GetPendingPriceChanges()
        {
            return _priceChangeRepository.GetAllPendingPriceChange(includeGradeInfo:false).ToList();
        }

        public List<PriceChangeState> GetJose(bool includeGradeInfo = false)
        {

            return _priceChangeRepository.GetScheduledPricesForAllSites(includeGradeInfo:false).ToList();

        }

        public PriceChange GetPriceChange(int priceChangeId)
        {
            return _priceChangeRepository.GetPriceChange(priceChangeId);
        }

        public PriceChangeState GetPriceChangeState(int PriceChangeStateId)
        {
            return _priceChangeRepository.GetPriceChangeState(PriceChangeStateId);
        }

    }
}
