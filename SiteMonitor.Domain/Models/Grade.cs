﻿using System.ComponentModel.DataAnnotations;

namespace SiteMonitor.Domain.Models
{
    public class Grade
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }
    }
}
